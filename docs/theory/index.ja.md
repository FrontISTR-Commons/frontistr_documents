# 理論

## 微小変形線形弾性静解析

ここでは微小変形理論に基づく弾性静解析についての定式化を示す。応力・ひずみ関係として線形弾性を仮定している。

### 基礎方程式

固体力学の平衡方程式、力学的境界条件、幾何学的境界条件（基本境界条件）は次式で与えられる（図2.1.1参照）。

\begin{equation}
\nabla \cdot \sigma + \overline{b} = 0 \quad in \ V
\label{eq:2.1.1}
\end{equation}

\begin{equation}
\sigma \cdot n = \overline{t} \quad on \ S_t
\label{eq:2.1.2}
\end{equation}

\begin{equation}
u = \overline{u} \quad on \ S_u
\label{eq:2.1.3}
\end{equation}

ここで、\(\sigma\)は応力、\(\overline{t}\)は表面力、\(S_t\)は物体力であり、\(S_t\)は力学的境界、\(S_u\)は幾何学的境界を>表す。

<figure markdown>
  ![固体力学における境界値問題(微小変形問題)](image/theory01_01.png){width="50%"}
  <figcaption>固体力学における境界値問題(微小変形問題)</figcaption>
</figure>
