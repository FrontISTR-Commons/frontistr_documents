# theory

## Infinitesimal Deformation Linear Elastic Static Analysis

In this section, the elastic static analysis is formulated on the basis of the infinitesimal deformation theory, which assumes linear elasticity as a stress-strain relationship.

### Basic equations

The equilibrium equation, mechanical boundary conditions, and geometric boundary conditions (basic boundary conditions) of solid mechanics are given by the following equations (see Fig. 2.1.1):

\begin{equation}
\nabla \cdot \sigma + \overline{b} = 0 \quad in \ V
\label{eq:2.1.1}
\end{equation}

\begin{equation}
\sigma \cdot n = \overline{t} \quad on \ S_t
\label{eq:2.1.2}
\end{equation}

\begin{equation}
u = \overline{u} \quad on \ S_u
\label{eq:2.1.3}
\end{equation}


where \(\sigma\), \(\overline{t}\) and \(S_t\) denote stress, surface force, and body force, respectively. \(S_t\) and \(S_u\) represent the geometric and mechanical boundaries, respectively.

<figure markdown>
  ![Boundary value problems in solid mechanics (micro-deformation problems)](image/theory01_01.png){width="50%"}
  <figcaption>Boundary value problems in solid mechanics (micro-deformation problems)</figcaption>
</figure>
