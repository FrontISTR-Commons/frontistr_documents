### メッシュデータ

有限要素メッシュを定義し、その材料データとセクションデータを定義します。(一部のみ)

```
!HEADER
 HECMW_Msh File generated by REVOCAP
!NODE
       1,    -1.22042,     2.23355,     1.65220
       2,    -1.27050,    -3.10529,     1.59209
...
!ELEMENT, TYPE=342
       1,    1157,    3549,    3321,    3739,   12629,   12627,   12626,   12628,   12631,   12630
       2,    8207,    3321,    3549,    3739,   12629,   12633,   12632,   12634,   12630,   12631
...
!MATERIAL, NAME=STEEL, ITEM=2
!ITEM=1, SUBITEM=2
210000.0, 0.3
!ITEM=2, SUBITEM=1
7.85e-6
!SECTION, TYPE=SOLID, EGRP=Solid0, MATERIAL=STEEL
!EGROUP, EGRP=Solid0
1
2
...
!END

```

- [!HEADER](../../keyword_reference/msh/header.ja.md): ヘッダーを指定
- [!NODE](../../keyword_reference/msh/node.ja.md): 節点の定義
- [!ELEMENT](../../keyword_reference/msh/element.ja.md): 要素の定義
- [!MATERIAL](../../keyword_reference/msh/material.ja.md): 材料の定義
- [!SECTION](../../keyword_reference/msh/section.ja.md): セクションの定義
- [!EGROUP](../../keyword_reference/msh/egroup.ja.md): 要素グループの定義
- [!END](../../keyword_reference/msh/end.ja.md): メッシュデータの終端を指定
