### Analysis control data `hinge.cnt`

```
#  Control File for FISTR
## Analysis Control
!VERSION                   
 3
!SOLUTION, TYPE=STATIC     
!WRITE,RESULT              
!WRITE,VISUAL              
## Solver Control
### Boundary Conditon
!BOUNDARY
 BND0, 1, 3, 0.000000      
!BOUNDARY
 BND1, 1, 3, 0.000000      
!CLOAD
 CL0, 1, 0.01000           
### Material
!MATERIAL, NAME=STEEL      
!ELASTIC                   
 210000.0, 0.3
!DENSITY                   
 7.85e-6
### Solver Setting
!SOLVER,METHOD=CG,PRECOND=1,ITERLOG=YES,TIMELOG=YES  # Solver control
 10000, 1
 1.0e-08, 1.0, 0.0
## Post Control
!VISUAL,metod=PSR          
!surface_num=1      
!surface 1           
!output_type=VTK     
!END             
```

- [!VERSION](../../keyword_reference/cnt/version.en.md): Specify the version of the file format
- [!SOLUTION, TYPE=STATIC](../../keyword_reference/cnt/solution.en.md): Specify the type of analysis
- [!WRITE,RESULT](../../keyword_reference/cnt/write_result.en.md): Specify the output of the result data
- [!WRITE,VISUAL](../../keyword_reference/cnt/write_visual.en.md): Specify the output of the visualization data
- [!BOUNDARY](../../keyword_reference/cnt/boundary.en.md): Specify the Dirichlet boundary conditions
- [!CLOAD](../../keyword_reference/cnt/cload.en.md): Specify the load condition
- [!MATERIAL](../../keyword_reference/cnt/material.en.md): Specify material property
- [!ELASTIC](../../keyword_reference/cnt/elastic.en.md): Specify elastic mateiral property
- [!DENSITY](../../keyword_reference/cnt/density.en.md): Specify density value
- [!SOLVER](../../keyword_reference/cnt/solver.en.md): Specify linear solver type
- [!VISUAL](../../keyword_reference/cnt/visual.en.md): Specify visualization method
- [!surface_num](../../keyword_reference/cnt/surface_num.en.md): Number of surfaces in one surface rendering 
- [!surface](../../keyword_reference/cnt/surface_num.en.md): Specify the contents of the surface
- [!output_type](../../keyword_reference/cnt/output_vis.en.md): Specify the type of the visualization file
- [!END](../../keyword_reference/cnt/end.en.md): Indicates the end of the analysis control