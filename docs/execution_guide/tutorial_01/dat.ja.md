### 全体制御データ `hecmw_ctrl.dat`

メッシュデータと解析制御データの入出力ファイルを指定します。

```
#
# for solver
#
!MESH, NAME=fstrMSH, TYPE=HECMW-ENTIRE
 hinge.msh
!CONTROL, NAME=fstrCNT
 hinge.cnt
!RESULT, NAME=fstrRES, IO=OUT
 hinge.res
!RESULT, NAME=vis_out, IO=OUT
 hinge_vis
```

- [!MESH](../../keyword_reference/dat/mesh.ja.md): 単一メッシュデータを指定
- [!CONTROL](../../keyword_reference/dat/control.ja.md): 解析制御データを指定
- [!RESULT](../../keyword_reference/dat/result.ja.md): 結果データを指定, 可視化データを指定
