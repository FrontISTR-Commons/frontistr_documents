#### 解析制御データ `hinge.cnt`

解析の種別、変位境界条件、集中荷重等を定義し、ソルバーやビジュアライザーの制御も指定します。

```
#  Control File for FISTR
## Analysis Control
!VERSION
 3
!SOLUTION, TYPE=STATIC
!WRITE,RESULT              
!WRITE,VISUAL
## Solver Control
### Boundary Conditon
!BOUNDARY
 BND0, 1, 3, 0.000000      
!BOUNDARY
 BND1, 1, 3, 0.000000      
!CLOAD
 CL0, 1, 0.01000           
### Material
!MATERIAL, NAME=STEEL
!ELASTIC                   
 210000.0, 0.3
!DENSITY
 7.85e-6
### Solver Setting
!SOLVER,METHOD=CG,PRECOND=1,ITERLOG=YES,TIMELOG=YES
 10000, 1
 1.0e-08, 1.0, 0.0
## Post Control
!VISUAL,metod=PSR
!surface_num=1
!surface 1                  
!output_type=VTK
!END
```

- [!VERSION](../../keyword_reference/cnt/version.ja.md):ファイルフォーマットのバージョンを指定
- [!SOLUTION](../../keyword_reference/cnt/solution.ja.md):解析の種別を指定
- [!WRITE,RESULT](../../keyword_reference/cnt/write_result.ja.md):結果データ出力の指定
- [!WRITE,VISUAL](../../keyword_reference/cnt/write_visual.ja.md):可視化データの出力を指定
- [!BOUNDARY](../../keyword_reference/cnt/boundary.ja.md):ディリクレ境界条件の設定
- [!CLOAD](../../keyword_reference/cnt/cload.ja.md):集中荷重の設定
- [!MATERIAL](../../keyword_reference/cnt/material.ja.md):材料物性の指定
- [!ELASTIC](../../keyword_reference/cnt/elastic.ja.md):弾性物質の定義
- [!DENSITY](../../keyword_reference/cnt/density.ja.md):質量密度の定義
- [!SOLVER](../../keyword_reference/cnt/solver.ja.md):ソルバーの制御
- [!VISUAL](../../keyword_reference/cnt/visual.ja.md):可視化手法の指定
- [!surface_num](../../keyword_reference/cnt/surface_num.ja.md):1つのサーフェースレンダリング内のサーフェース数
- [!surface](../../keyword_reference/cnt/surface_num.ja.md):サーフェースの内容の指定
- [!output_type](../../keyword_reference/cnt/output_type.ja.md):可視化ファイルの型の指定
- [!END](../../keyword_reference/cnt/end.ja.md):解析制御データの終わりを示す