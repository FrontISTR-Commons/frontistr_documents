### Global control data `hecmw_ctrl.dat`

```
#
# for solver
#
!MESH, NAME=fstrMSH, TYPE=HECMW-ENTIRE 
 hinge.msh
!CONTROL, NAME=fstrCNT  
 hinge.cnt
!RESULT, NAME=fstrRES, IO=OUT 
 hinge.res
!RESULT, NAME=vis_out, IO=OUT 
 hinge_vis
```

-[!MESH](../../keyword_reference/dat/mesh.en.md): Specify a single mesh data 
-[!CONTROL](../../keyword_reference/dat/control.en.md): Specify analysis control data
-[!RESULT](../../keyword_reference/dat/result.en.md): Specify the result data