## Linear Static Analysis (Elasticity)

For this analysis, the data in `tutorial/01_elastic_hinge` is used.

### Analysis model

The analysis object is a hinge part, and the geometry is shown in Figure 4.1.1 and the mesh data in Figure 4.1.2.

 | Item               | Description                           | Notes                 | Reference |
 |--------------------|---------------------------------------|-----------------------|-----------|
 | Type of analysis   | Linear static analysis                | !SOLUTION,TYPE=STATIC |           |
 | Number of nodes    | 84,056                                |                       |           |
 | Number of elements | 49,871                                |                       |           |
 | Element type       | 10-node tetrahedron quadratic element | !ELEMENT,TYPE=342     |[Element Library](../analysis/analysis_02.html#_2)|
 | Material Name      | STEEL                                 | !ELASTIC              |[Material Data](../analysis/analysis_02.html#12)  |
 | Material property  | ELASTIC                               |                       |           |
 | Boudary condition  | Restrained, Concentrated force        |                       |           |
 | Matrix solution    | CG/SSOR                               | !SOLVER,METHOD=CG,PRECOND=1 |     |

![Shape of the hinge part](./media/tutorial01_01.png){.center width="350px"}
<div style="text-align: center;">
Fig. 4.1.1 : Shape of the hinge part
</div>

![Mesh data of the hinge part](./media/tutorial01_02.png){.center width="350px"}
<div style="text-align: center;">
Fig. 4.1.2 : Mesh data of the hinge part
</div>

### Files

Extract the source code of FrontISTR and go to the directory of this example to see if you can find the files necessary for analysis.

 | File name        | Type                  | Role                                                                          |
 |------------------|-----------------------|-------------------------------------------------------------------------------|
 | `hecmw_ctrl.dat` | Global control data   | Specifies the input and output files for mesh data and analysis control data. |
 | `hinge.cnt`      | Analysis control data | Define the type of analysis, displacement boundary conditions, concentrated loads, etc., and also specify control of the solver and visualizer. |
 | `hinge.msh`      | Mesh data             | Defines a finite element mesh and defines its material and section data       |

```
$ tar xvf FrontISTR.tar.gz
$ cd FrontISTR/tutorial/01_elastic_hinge
$ ls
hecmw_ctrl.dat  hinge.cnt  hinge.msh
```

A stress analysis is performed to constrain the displacement of the constrained surface shows in Figure 4.1.1 and to apply concentrated loads to the forcing surface.

The overall control data and analytical control data are shown below.

### Analysis procedure

Execute the FrontISTR command `fistr1`.

```
$ fistr1 -t 4
(Runs in 4 threads.)
```

```
##################################################################
#                         FrontISTR
#
##################################################################
---
version:    5.1.0
git_hash:   acab000c8c633b7b9d596424769e14363f720841
build:
  date:     2020-10-05T07:39:55Z
  MPI:      enabled
  OpenMP:   enabled
  option:   "-p --with-tools --with-refiner --with-metis --with-mumps --with-lapack --with-ml --with-mkl "
  HECMW_METIS_VER: 5
execute:
  date:       2020-10-07T10:01:16+0900
  processes:  1
  threads:    4
  cores:      4
  host:
    0: flow-p06
---
...
 Step control not defined! Using default step=1
 fstr_setup: OK
 Start visualize PSF 1 at timestep 0

 loading step=    1
 sub_step= 1,   current_time=  0.0000E+00, time_inc=  0.1000E+01
 loading_factor=    0.0000000   1.0000000
### 3x3 BLOCK CG, SSOR, 1
      1    1.903375E+00
      2    1.974378E+00
      3    2.534627E+00
...
...
   2967    1.080216E-08
   2968    1.004317E-08
   2969    9.375729E-09
### Relative residual = 9.39429E-09

### summary of linear solver
      2969 iterations      9.394286E-09
    set-up time      :     1.953022E-01
    solver time      :     5.704201E+01
    solver/comm time :     5.145826E-01
    solver/matvec    :     2.306329E+01
    solver/precond   :     2.632665E+01
    solver/1 iter    :     1.921253E-02
    work ratio (%)   :     9.909789E+01

 Start visualize PSF 1 at timestep 1
### FSTR_SOLVE_NLGEOM FINISHED!

 ====================================
    TOTAL TIME (sec) :     59.99
           pre (sec) :      0.71
         solve (sec) :     59.29
 ====================================
 FrontISTR Completed !!
```

The analysis is completed when `FrontISTR Completed !!` is displayed, the analysis is done.

### Analysis results

Once the analysis is complete, several new files will be created.

```
$ ls
0.log       hecmw_ctrl.dat  hinge.res.0.0            hinge_vis_psf.0001
FSTR.dbg.0  hecmw_vis.ini   hinge.res.0.1            hinge_vis_psf.0001.pvtu
FSTR.msg    hinge.cnt       hinge_vis_psf.0000
FSTR.sta    hinge.msh       hinge_vis_psf.0000.pvtu
```

The `*.res.*` is the result data of FrontISTR, which can be displayed by REVOCAP_PrePost and so on.

The `*_vis_*` is called visualization data, and can be displayed by general-purpose visualization software. In this example, the data is output in VTK format, so it can be displayed using ParaView and other visualization software.

A contour plot for the Mises stresses is created by REVOCAP_PrePost and shown in Figure 4.1.3.
A portion of the analysis results log file  is also shown below as numerical data for the analysis results.

![Analysis results of Mises stress](./media/tutorial01_03.png){.center width="350px"}
<div style="text-align: center;">
Fig. 4.1.3 : Analysis results of Mises stress
</div>

#### Analysis result log `0.log`

```
 fstr_setup: OK
#### Result step=     0
 ##### Local Summary @Node    :Max/IdMax/Min/IdMin####
 //U1    0.0000E+00         1  0.0000E+00         1
 //U2    0.0000E+00         1  0.0000E+00         1
 //U3    0.0000E+00         1  0.0000E+00         1
 //E11   0.0000E+00         1  0.0000E+00         1
 //E22   0.0000E+00         1  0.0000E+00         1
 //E33   0.0000E+00         1  0.0000E+00         1
 //E12   0.0000E+00         1  0.0000E+00         1
 //E23   0.0000E+00         1  0.0000E+00         1
 //E31   0.0000E+00         1  0.0000E+00         1
 //S11   0.0000E+00         1  0.0000E+00         1
 //S22   0.0000E+00         1  0.0000E+00         1
 //S33   0.0000E+00         1  0.0000E+00         1
 //S12   0.0000E+00         1  0.0000E+00         1
 //S23   0.0000E+00         1  0.0000E+00         1
 //S31   0.0000E+00         1  0.0000E+00         1
 //SMS   0.0000E+00         1  0.0000E+00         1
 ##### Local Summary @Element :Max/IdMax/Min/IdMin####
 //E11   0.0000E+00         1  0.0000E+00         1
```
