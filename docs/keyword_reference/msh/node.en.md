
Definition of node coordinates

1st Line

```
!NODE [, optional parameter]
```

|Parameter|                              |
|---------|------------------------------|
|`SYSTEM` |Coordinate system (omissible) |
|`NGRP`   |Node group name (omissible)   |
|`INPUT`  |External file name (omissible)|

|Parameter Name|Parameter Value |Contents                                                                            |
|--------------|----------------|------------------------------------------------------------------------------------|
|`SYSTEM`      |R               |Cartesian coordinate system (Default value)                                         |
|              |C               |Cylindrical coordinate system                                                       |
|`NGRP`        |&lt;ngrp&gt;    |Node group name (omissible)                                                         |
|`INPUT`       |&lt;filename&gt;|External file name (omissible), can also be used together with the 2nd line or later|

2nd Line or later

```
(2nd Line) NODE_ID, Xcoord, Ycoord, Zcoord
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents    |
|--------------|------------|------------|
|`NODE_ID`     |I           |Node number |
|`Xcoord`      |R           |X coordinate|
|`Ycoord`      |R           |Y coordinate|
|`Zcoord`      |R           |Z coordinate|

> Note:
>
> - When node coordinates including the punctuation mark is omitted, the value will become "0.0".
> - When an already defined node is redefined, the contents will be updated and a warning message will be displayed.
> - The node which is not referred to in "`!ELEMENT`" will be excluded.
> - The node defined in "`!ELEMENT`" must be defined before "`!ELEMENT`".

Example of Use

```
!NODE, NGRP=TEST
1, 0.0, 0.0, 0.5
2, 0.0, 0.0, 1.0
3, 0.0,,1.5                    ----- Y coordinate is "0.0"
4,                             ----- X, Y and Z coordinates are "0.0"
```

