
End of mesh data

When this header is displayed, the reading of the mesh data is completed.

#### 1st Line

```
!END
```

|Parameter|
|---------|
|N/A      |

#### 2nd Line or later

|Parameter|
|---------|
|N/A      |


