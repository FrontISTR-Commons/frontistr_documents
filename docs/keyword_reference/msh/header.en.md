
Title of mesh data

1st Line

```
!HEADER
```

|Parameter|
|---------|
|N/A      |

2nd Line or later

```
(2nd Line) TITLE
```

|Parameter Name|Attributions|Contents    |
|--------------|------------|------------|
|`TITLE`       |C           |Header title|

Example of Use

```
!HEADER
Mesh for CFD Analysis
```

> Note:
>
> - Omissible
> - Although the header can use multiple lines, it can be recognized as a header up to the 127th column of the first line.
> - When "`!HEADER`" is defined multiple times, the contents will be updated and a warning message will be displayed.

