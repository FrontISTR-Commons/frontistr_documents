
ステップ内での荷重条件を与える変数の時間変化の指定

```
!AMPLITUDE, NAME=<name> [, optional parameter]
VAL1, T1, VAL2, T2, VAL3, T3

#（一行に四項まで）
```

| パラメータ  |                       |
|:------------|:----------------------|
|`NAME`       |名前 (必須)            |
|`DEFINITION` |タイプ (省略可)        |
|`TIME`       |時間の種類 (省略可)    |
|`VALUE`      |値の種類 (省略可)      |
|`INPUT`      |外部ファイル名 (省略可)|

| パラメータ名 | パラメータ値     | 内 容                                            |
|:-------------|:-----------------|:-------------------------------------------------|
| `NAME`       | &lt;name&gt;     | `AMPLITUDE`名                                    |
| `DEFINITION` | TABULAR          | デフォルト (現版ではデフォルトのみ)              |
| `TIME`       | STEP TIME        | デフォルト (現版ではデフォルトのみ)              |
| `VALUE`      | RELATIVE         | 相対値 (デフォルト)                              |
|              | ABSOLUTE         | 絶対値                                           |
| `INPUT`      | &lt;filename&gt; | 外部ファイル名 (省略可)、2行目以降との併用も可能 |

| 変数名 | 属性 | 内容             |
|:-------|:-----|:-----------------|
| `VAL1` | R    | 時間T1における値 |
| `T1`   | R    | 時間T1           |
| `VAL2` | R    | 時間T2における値 |
| `T2`   | R    | 時間T2           |
| `VAL3` | R    | 時T3における値   |
| `T3`   | R    | 時間T3           |

