
Definition of material physical properties.

When the physical properties depend on the temperature, table input can be performed for each compatible temperature.

The table input can also be performed for the relationship of the stress strain in a stress analysis.

1st Line

```
!MATERIAL, NAME=<name> [, optional parameter]
```

|Parameter|                                                                       |
|---------|-----------------------------------------------------------------------|
|`NAME`   |Material name (mandatory)                                              |
|`ITEM`   |Number of physical property items (omissible, becomes "1" when omitted)|
|`INPUT`  |External file name (omissible)                                         |

|Parameter Name|Parameter Value |Contents                                            |
|--------------|----------------|----------------------------------------------------|
|`NAME`        |&lt;name&gt;    |Material name                                       |
|`ITEM`        |&lt;ITEMnum&gt; |Number of physical property items by user definition|
|`INPUT`       |&lt;filename&gt;|External file name (omissible), can also be used together with the 2nd line or later|

2nd Line or later

```
(2nd Line) !ITEM=1, SUBITEM=<k>
(3rd Line) VAL1-1-1, VAL1-1-2, ... VAL1-1-k, TEMP1-1
(4th Line) VAL1-2-1,VAL1-2-2, ... VAL1-2-k, TEMP1-2
・・
(L+2nd Line) VAL1-L-1,VAL1-L-2, ... VAL1-L-k, TEMP1-L
Hereinafter, the definition is repeated until "!ITEM=<ITEMnum>".
```

|Subparameter (for "!ITEM") |                                           |
|---------------------------|-------------------------------------------|
|`SUBITEM`                  | Number of sub physical property items defined in each of the physical property items(omissible, becomes "1" when omitted, used to define anisotropy and etc.)|

|Subparameter Name|Parameter Value   |Contents                                                |
|-----------------|------------------|--------------------------------------------------------|
|`SUBITEM`        |&lt;subITEMnum&gt;|Number of sub physical property items by user definition|

**[When the m-th physical property depends on the temperature]**

When the number of items of the temperature dependent table is N, input as follows:

```
!ITEM=m, SUBITEM=k
VALm1-1, ..., VALm1-k, TEMPm1
VALm2-1, ..., VALm2-k, TEMPm2
...
VALmN-1, ..., VALmN-k, TEMPm-N
```

|Parameter Name|Attributions|Contents                                       |
|--------------|------------|-----------------------------------------------|
|`VALmn-k`     |R           |Physical property value (Temperature dependent)|
|`TEMPmn`      |R           |Compatible temperature                         |

Must be input as `TEMPm1` < `TEMPm2` < ... < `TEMPmN`.

`VALm1` is used when the temperature is `TEMPm1` or below, and `VALmN` is used when the temperature is `TEMPmN` or more.

**[When the m-th physical property does not depend on the temperature]**

```
!ITEM=m, SUBITEM=k
VALm1-1, ..., VALm1-k
VALm2-1, ..., VALm2-k
...
VALmN-1, ..., VALmN-k
```

|Parameter Name|Attributions|Contents                                           |
|--------------|------------|---------------------------------------------------|
|VALmn-k       |R           |Physical property value (not temperature dependent)|

> Note:
>
> - When the material name is duplicated, an error will occur.
> - When the MATERIAL referred to in the "`!SECTION`" option is not defined, an error will occur.
> - The value used to input the physical property for each element using the parameter "`MATITEM`" in the "`!ELEMENT`" option, is used preferentially. In this case, the physical property value input using the "`!MATERIAL`" option will not be used.
> - When the number of "!ITEM=m" suboptions and the number of parameters "`ITEM`" is not consistent, or when there is an undefined suboption, and error will occur.
> - The "`!ITEM=m`" suboption does not have to be in order from the smaller m.
> - When using the "`!SUBITEM=k`" suboption and the temperature dependency, the omitted value will become "0.0".
> - When using the temperature dependency, it must be defined in order from the lower temperature.
> - When using the temperature dependency, if the same temperature is used twice or more, an error will occur.

Example of Use

```
!MATERIAL, NAME= STEEL, ITEM= 2
!ITEM=1                            ----- No temperature dependency
35.0
!ITEM=2
40.0, 0.0
45.0, 100.0
50.0, 200.0
!MATERIAL, NAME= CUPPER            ----- Number of items = 1 (Default value)
!ITEM=1                            ----- No temperature dependency
80.0
```
Incorrect Example of Use

- Example 1: [Number of parameter "`ITEM`" and "`!ITEM=m`" suboptions are not consistent -1]

```
!MATERIAL, NAME= STEEL, ITEM= 2
!ITEM=3
20.0
!ITEM=1
35.0
!ITEM= 2
40.0
```

- Example 2: [Number of parameter "`ITEM`" and "`!ITEM=m`" suboptions are not consistent -2]

```
!MATERIAL, NAME= STEEL, ITEM= 3
!ITEM=3
20.0
!ITEM= 2
40.0
!MATERIAL, NAME= CUPPER
...
```

Elastic Static Analysis and Eigenvalue Analysis

```
!MATERIAL, NAME=<name>, ITEM=<ITEMnum>
!ITEM=1, SUBITEM=2
<Yang_modulus>, <Poisson_ratio>
!ITEM=2
<Density>
!ITEM=3
<Expansion_coeff>
```

|Parameter Name|Parameter Value|Contents                                               |
|--------------|---------------|-------------------------------------------------------|
|`NAME`        |&lt;name&gt;   |Compatible to material name, and MATERIAL of `!SECTION`|
|`ITEM`        |&lt;ITEMnum&gt;|Number of physical property items by user definition (1 or more)<br/> &lt;Yang_modulus&gt; ... Young's modulus (mandatory)<br/> &lt;Poisson_ratio&gt; ... Poisson's ratio (mandatory)<br/> &lt;Density&gt; ... Mass density (mandatory when ITEMnum=3)<br/> &lt;Expansion_coeff&gt; ... Coefficient of linear expansion (when ITEMnum=3)|

Example

```
!! Intention of defining three types of property values
!! in the material of material name M1
!MATERIAL, NAME=M1, ITEM=3

!! The Young's modulus and Poisson's ratio is defined in !ITEM=1 (mandatory)
!ITEM=1, SUBITEM=2
4000., 0.3

!! The mass density must be defined in !ITEM=2 (mandatory in the case of ITEM=3)
!ITEM=2
8.0102E‐10

!! The coefficient of linear expansion must be defined in !ITEM=3
!ITEM=3
1.0E‐5
```

Heat Conduction Analysis

In the case of link, plane surface, solid and three-dimensional plate elements

```
!MATERIAL, NAME=<name>, ITEM=3
!ITEM=1, SUBITEM=2
<Density>, <Temperature>
!ITEM=2, SUBITEM=2
<Specific_heat>, <Temperature>
!ITEM=3, SUBITEM=2
<Conductivity>, <Temperature>
```

|Parameter Name|Parameter Value|Contents                                               |
|--------------|---------------|-------------------------------------------------------|
|`NAME`        |&lt;name&gt;   |Compatible to material name, and MATERIAL of `!SECTION`|
|`ITEM`        |&lt;ITEMnum&gt;|Number of physical property items by user definition (always 3)<br/>&lt;Density&gt; ... Density<br/>&lt;Specific_heat&gt; ... Specific heat<br/>&lt;Conductivity&gt; ... Thermal conductivity<br/>&lt;Temperature&gt; ... Temperature|

Example

```
!! Intention of defining three types of property
!! values in the material of material name M1
!MATERIAL, NAME=M1, ITEM=3

!! The density and temperature are defined in !ITEM=1 (mandatory)
!ITEM=1, SUBITEM=1
7850., 300.
7790., 500.
7700., 800.

!! The specific heat and temperature are defined in !ITEM=2 (mandatory)
!ITEM=2, SUBITEM=1
0.465, 300.
0.528, 500.
0.622, 800.

!! The thermal conductivity and temperature are defined in !ITEM=3 (mandatory)
!ITEM=3
43., 300.
38.6, 500.
27.7, 800.
```

In the case of interface element

Defined in the `!SECTION` header. (Material data is not required)

Example

```
!! Definition of section
!SECTION, TYPE=INTERFACE, EGRP=GAP
1.0, 20.15, 8.99835E-9, 8.99835E-9
```

In the above `!SECTION`, the gap parameter of the element belonging to the "`group name = GAP`" in the interface element is defined.

- 1st parameter : Gap width
- 2nd parameter : Gap heat transfer coefficient
- 3rd parameter : Gap radiation factor 1
- 4th parameter : Gap radiation factor 2

Reference

```
program TEST
use hecmw
implicit REAL*8 (A-H,O-Z)
type (hecmwT_local_mesh) :: hecMESH

!C
!C    !MATERIAL, NAME=SUS304, ITEM=3
!C    !ITEM=1, SUBITEM= 3
!C      100.0, 200.0, 300.0, 0.00
!C      101.0, 210.0, 301.0, 1.00
!C      102.0, 220.0, 302.0, 2.00
!C      103.0, 230.0, 303.0, 3.00
!C    !ITEM=3, SUBITEM= 2
!C      1000.0, , 0.00
!C      1001.0, 1., 1.00
!C      1002.0, 2., 2.00
!C      1003.0, 3., 3.00
!C    !ITEM=2
!C      5000.0
!C
!C    !MATERIAL, NAME=FEC, ITEM=2
!C    !ITEM=1, SUBITEM= 3
!C      2100.0, 2200.0, 2300.0, 0.00
!C      2101.0, 2210.0, 2301.0, 1.00
!C      2102.0, 2220.0, 2302.0, 2.00
!C      2103.0, 2230.0, 2303.0, 3.00
!C      3103.0, 3230.0, 2304.0, 4.00
!C    !ITEM=2
!C      6000.0, 10.0
!C      6500.0, 30.0
!C

hecMESH%material%n_mat = 2

nn= hecMESH%material%n_mat
allocate (hecMESH%material%mat_name(nn))

hecMESH%material%mat_name(1)= 'SUS304'
hecMESH%material%mat_name(2)= 'FEC'

nn= hecMESH%material%n_mat
allocate (hecMESH%material%mat_ITEM_index(0:nn))
hecMESH%material%mat_ITEM_index(0)= 0
hecMESH%material%mat_ITEM_index(1)= 3
hecMESH%material%mat_ITEM_index(2)= hecMESH%material%mat_ITEM_index(1) + 2

hecMESH%material%n_mat_ITEM= hecMESH%material%mat_ITEM_index(hecMESH%material%n_mat)

nn= hecMESH%material%n_mat_ITEM
allocate (hecMESH%material%mat_subITEM_index(0:nn))

hecMESH%material%mat_subITEM_index(0)= 0
hecMESH%material%mat_subITEM_index(1)= 3
hecMESH%material%mat_subITEM_index(2)= hecMESH%material%mat_subITEM_index(1) + 1
hecMESH%material%mat_subITEM_index(3)= hecMESH%material%mat_subITEM_index(2) + 2
hecMESH%material%mat_subITEM_index(4)= hecMESH%material%mat_subITEM_index(3) + 3
hecMESH%material%mat_subITEM_index(5)= hecMESH%material%mat_subITEM_index(4) + 1

hecMESH%material%n_mat_subITEM=
&        hecMESH%material%mat_subITEM_index(hecMESH%material%n_mat_ITEM)

nn= hecMESH%material%n_mat_subITEM
allocate (hecMESH%material%mat_TABLE_index(0:nn))
hecMESH%material%mat_TABLE_index( 0)= 0
hecMESH%material%mat_TABLE_index( 1)= 4
hecMESH%material%mat_TABLE_index( 2)= hecMESH%material%mat_TABLE_index( 1) + 4
hecMESH%material%mat_TABLE_index( 3)= hecMESH%material%mat_TABLE_index( 2) + 4
hecMESH%material%mat_TABLE_index( 4)= hecMESH%material%mat_TABLE_index( 3) + 1
hecMESH%material%mat_TABLE_index( 5)= hecMESH%material%mat_TABLE_index( 4) + 4
hecMESH%material%mat_TABLE_index( 6)= hecMESH%material%mat_TABLE_index( 5) + 4
hecMESH%material%mat_TABLE_index( 7)= hecMESH%material%mat_TABLE_index( 6) + 5
hecMESH%material%mat_TABLE_index( 8)= hecMESH%material%mat_TABLE_index( 7) + 5
hecMESH%material%mat_TABLE_index( 9)= hecMESH%material%mat_TABLE_index( 8) + 5
hecMESH%material%mat_TABLE_index(10)= hecMESH%material%mat_TABLE_index( 9) + 2

hecMESH%material%n_mat_TABLE=
&        hecMESH%material%mat_TABLE_index(hecMESH%material%n_mat_subITEM)

nn= hecMESH%material%n_mat_TABLE
allocate (hecMESH%material%mat_VAL (nn))
allocate (hecMESH%material%mat_TEMP(nn))

hecMESH%material%mat_VAL = 0.d0
hecMESH%material%mat_TEMP= 0.d0

hecMESH%material%mat_VAL ( 1)= 100.0d0
hecMESH%material%mat_TEMP( 1)=   0.0d0
hecMESH%material%mat_VAL ( 2)= 101.0d0
hecMESH%material%mat_TEMP( 2)=   1.0d0
hecMESH%material%mat_VAL ( 3)= 102.0d0
hecMESH%material%mat_TEMP( 3)=   2.0d0
hecMESH%material%mat_VAL ( 4)= 103.0d0
hecMESH%material%mat_TEMP( 4)=   3.0d0

hecMESH%material%mat_VAL ( 5)= 200.0d0
hecMESH%material%mat_TEMP( 5)=   0.0d0

hecMESH%material%mat_VAL (13)= 5000.0d0

hecMESH%material%mat_VAL (14)= 1000.0d0
hecMESH%material%mat_TEMP (14)=   0.0d0
hecMESH%material%mat_VAL (15)= 1001.0d0
hecMESH%material%mat_TEMP (15)=   1.0d0
hecMESH%material%mat_VAL (16)= 1002.0d0
hecMESH%material%mat_TEMP (16)=   2.0d0
hecMESH%material%mat_VAL (17)= 1003.0d0
hecMESH%material%mat_TEMP (17)=   3.0d0

hecMESH%material%mat_VAL (18)=  0.0d0
hecMESH%material%mat_TEMP (18)= 0.0d0
hecMESH%material%mat_VAL (19)=  1.0d0
hecMESH%material%mat_TEMP (19)= 1.0d0
hecMESH%material%mat_VAL (20)=  2.0d0
hecMESH%material%mat_TEMP (20)= 2.0d0
hecMESH%material%mat_VAL (21)=  3.0d0
hecMESH%material%mat_TEMP (21)= 3.0d0

hecMESH%material%mat_VAL (22)= 2100.0d0
hecMESH%material%mat_TEMP (22)=   0.0d0
hecMESH%material%mat_VAL (23)= 2101.0d0
hecMESH%material%mat_TEMP (23)=   1.0d0
hecMESH%material%mat_VAL (24)= 2102.0d0
hecMESH%material%mat_TEMP (24)=   2.0d0
hecMESH%material%mat_VAL (25)= 2103.0d0
hecMESH%material%mat_TEMP (25)=   3.0d0
hecMESH%material%mat_VAL (26)= 3103.0d0
hecMESH%material%mat_TEMP (26)=   4.0d0

write (*,'(a,i10)') '%n_mat_ITEM ', hecMESH%material%n_mat_ITEM
write (*,'(a,i10)') '%n_mat_subITEM', hecMESH%material%n_mat_subITEM
write (*,'(a,i10)') '%n_mat_TABLE ', hecMESH%material%n_mat_TABLE

end program TEST
```

