
Definition of node group

1st Line

```
!NGROUP, NGRP=<ngrp> [, optional parameter]
```

|Parameter |                                                                     |
|----------|---------------------------------------------------------------------|
|`NGRP`    |Node group name (mandatory)                                          |
|`GENERATE`|Automatic generation of nodes belonging to the node group (omissible)|
|`INPUT`   |External file name (omissible)                                       |

|Parameter Name|Parameter Value |Contents                                        |
|--------------|----------------|------------------------------------------------|
|`NGRP`        |&lt;ngrp&gt;    |Node group name GENERATE N/A Automatic generation of nodes belonging to the node group|
|`INPUT`       |&lt;filename&gt;|External file name (omissible), can also be used together with the 2nd line or later|

2nd Line or later (when `GENERATE` is not used)

```
(2nd Line) nod1, nod2, nod3
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents                               |
|--------------|------------|---------------------------------------|
|`nodX`        |I           |Node number belonging to the node group|

2nd Line or later (when `GENERATE` is used)

```
(2nd Line) nod1, nod2, nod3
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents                                 |
|--------------|------------|-----------------------------------------|
|`nod1`        |I           |First node number in the node group      |
|`nod2`        |I           |Last node number in the node group       |
|`nod3`        |I           |Node number increment (omissible, number becomes `nod3=1` when ommited)|

> Note:
>
> - Any number of nodes can be inserted in one line. Any number of lines can be inserted until the next option starts.
> - It is necessary to define the nodes to be specified before "`!NGROUP`".
> -  The node not defined in the "`!NODE`" option will be excluded, and a warning message will be displayed.
> - When the specified node exists in the same group, it will be ignored and a warning message will be displayed.
> - All the nodes belong to the node group named "`ALL`" (generated automatically).
> - One group can be defined by dividing into multiple groups.

Example of Use

```
!NGROUP, NGRP=NA01
1, 2, 3, 4, 5, 6
101, 102
!NGROUP, NGRP=NA02
101, 102
!NGROUP, NGRP=NA01                   ----- "501 and 505" are added to group "NA01".
501, 505
!NGROUP, NGRP=NA02                   ----- "501 and 505" are added to group "NA02".
501, 505
!NGROUP, NGRP=NA04,GENERATE          ----- "301, 303, 305, 307, 309, 311, 312, 313"
301, 309, 2                               are added to group "NA04".
311, 313
```

