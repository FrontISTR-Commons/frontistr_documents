
Definition of section

1st Line

```
!SECTION, TYPE=<type>, EGRP=<egrp> [, optional parameter]
```

|Parameter  |                                                                             |
|-----------|-----------------------------------------------------------------------------|
|`TYPE`     |Section type (mandatory)                                                     |
|`EGRP`     |Element group name (mandatory)                                               |
|`MATERIAL` |User defined material name (mandatory)                                       |
|`SECOPT`   |Auxiliary parameter for the element type (omissible, becomes =0 when omitted)|
|`INPUT`    |External file name (omissible)                                               |

| Parameter Name | Parameter Value  | Contents                                                                             |
|----------------|------------------|--------------------------------------------------------------------------------------|
| `TYPE`         | `SOLID`          | Rod, triangular, quadrilateral, tetrahedral, pentahedral, hexadedral elements        |
|                | `SHELL`          | Shell element                                                                        |
|                | `BEAM`           | Beam element                                                                         |
|                | `INTERFACE`      | Interface element                                                                    |
| `EGRP`         | &lt;egrp&gt;     | Element group name                                                                   |
| `MATERIAL`     | &lt;material&gt; | Material name defined by user                                                        |
| `SECOPT`       | &lt;secopt&gt;   | = 0: Not specified, plane stress                                                     |
|                |                  | = 1: Plane strain                                                                    |
|                |                  | = 2: Axial symmetry                                                                  |
|                |                  | = 10: 0 + reduced integration                                                        |
|                |                  | = 11: 1 + reduced integration                                                        |
|                |                  | = 12: 2 + reduced integration                                                        |
| `INPUT`        | &lt;filename&gt; | External file name (omissible), can also be used together with the 2nd line or later |

2nd Line or later

**In the case of [`TYPE=SOLID`]**

```
(2nd Line) THICKNESS
```

|Parameter Name|Attributions|Contents                               |
|--------------|------------|---------------------------------------|
|`THICKNESS`   |R           |Element thickness, cross-sectional area|

In the case of "`TYPE=SOLID`", the "`THICKNESS`" can be omitted, and default value (1.0) is inserted.

**In the case of [`TYPE=SHELL`]**

```
(2nd Line) THICKNESS, INTEGPOINTS
```

|Parameter Name|Attributions|Contents                                         |
|--------------|------------|-------------------------------------------------|
|`THICKNESS`   |R           |Shell cross section thickness                    |
|`INTEGPOINTS` |I           |Integral point in shell cross sectional direction|

**In the case of [`TYPE=BEAM`]**

```
(2nd Line) vx,vy,vz,area,Iyy,Iz,Jx
```

|Parameter Name|Attributions|Contents                          |
|--------------|------------|----------------------------------|
|vx,vy,vz      |R           |Direction cosine of reference axis|
|area          |R           |Area of cross section             |
|Iyy, Izz      |R           |Second moment of cross section    |
|Jx            |R           |Torsion constant of cross section |

**In the case of [`TYPE=INTERFACE`]**

```
(2nd Line) THICKNESS, GAPCON, GAPRAD1, GAPRAD2
```

|Parameter Name|Attributions|Contents                                           |
|--------------|------------|---------------------------------------------------|
|`THICKNESS`   |R           |Cross-sectional thickness                          |
|`GAPCON`      |R           |Gap heat transfer coefficient (0 when omitted)     |
|`GAPRAD1`     |R           |Gap radiant heat transfer factor-1 (0 when omitted)|
|`GAPRAD2`     |R           |Gap radiant heat transfer factor-2 (0 when omitted)|

> Note:
>
> - When the parameter "`TYPE`" is not consistent with the element type, an error will occur.
> - When there is an element without `SECTION` information, an error will occur.
> - When the section name is duplicated, an error will occur.

Example of Use

```
!SECTION, EGRP=SOLID1, TYPE=SOLID, MATERIAL=STEEL
!SECTION, EGRP=SHELL2, TYPE=SHELL, MATERIAL=STEEL
1.0, 5
```

