
Definition of elements

1st Line

```
!ELEMENT, TYPE=<type> [, optional parameter]
```

|Parameter |                              |
|----------|------------------------------|
|`TYPE`    |Element type (mandatory)      |
|`EGRP`    |Element group name (omissible)|
|`MATITEM` |Number of physical property items when defining the material physical properties for each element (not used when defining physical properties for each section)|
|`INPUT`   |External file name (omissible)|

| Parameter Name | Parameter Value  | Contents                                                                             |
|----------------|------------------|--------------------------------------------------------------------------------------|
| `TYPE`         | 111              | Rod, link element (Linear)                                                           |
|                | 231              | Triangular element (Linear)                                                          |
|                | 232              | Triangular element (Quadratic)                                                       |
|                | 241              | Quadrilateral element (Linear)                                                       |
|                | 242              | Quadrilateral element (Quadratic)                                                    |
|                | 301              | Truss element (Linear)                                                               |
|                | 341              | Tetrahedral element (Linear)                                                         |
|                | 342              | Tetrahedral element (Quadratic)                                                      |
|                | 351              | Triangular prism element (Linear)                                                    |
|                | 352              | Triangular prism element (Quadratic)                                                 |
|                | 361              | Hexahedral element (Linear)                                                          |
|                | 362              | Hexahedral element (Quadratic)                                                       |
|                | 541              | Interface element (Quadrilateral cross section, Linear)                              |
|                | 611              | Beam element(Linear)                                                                 |
|                | 641              | Beam element(Linear, with 3-dof nodes)                                               |
|                | 731              | Triangular shell element (Linear)                                                    |
|                | 741              | Quadrilateral shell element (Linear)                                                 |
|                | 743              | Quadrilateral shell element (Quadratic)                                              |
|                | 761              | Triangular shell element (Linear, with 3-dof nodes)                                  |
|                | 781              | Quadrilateral shell element (Linear, with 3-dof nodes)                               |
| `EGRP`         | &lt;egrp&gt;     | Element group name (omissible)                                                       |
| `INPUT`        | &lt;filename&gt; | External file name (omissible), can also be used together with the 2nd line or later |

2nd Line or later

```
(2nd Line) ELEM_ID, nod1, nod2, nod3, ..., MAT1, MAT2, ...
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents                                |
|--------------|------------|----------------------------------------|
|`ELEM_ID`     |I           |Element number                          |
|`nodX`        |I           |Connectivity                            |
|`MATy`        |R           |Physical Property value for each element|

> Note:
>
> - For details of the element types and connectivity, refer to "Chapter 4 Element Library".
> - The node specified by the connectivity must be defined before "`!ELEMENT`".
> - The element numbers do not have to be continued.
> - The "`!ELEMENT`" option can be defined any number of times.
> - The element number must be a natural number. This can not be omitted.
> - When the same element number is used repeatedly, the value input last will be used. In this case, a warning message will be output.
> - Undefined nodes can not be used for connectivity.
> - The definition of one element can be described in multiple lines.

Example of Use

```
!ELEMENT, TYPE=231
1, 1, 2, 3
2, 4, 8, 5
4, 6, 7, 8
!ELEMENT, TYPE=361, EGRP=A
101, 101, 102, 122, 121, 201, 202, 222, 221
102, 102, 103, 123, 122, 202, 203, 223, 222
103, 103, 104, 124, 123, 203, 204, 224, 223
```

