
Definition of element group

```
!EGROUP, EGRP=<egrp> [, optional parameter]
```

|Parameter |                                                                        |
|----------|------------------------------------------------------------------------|
|`EGRP`    |Element group name (mandatory)                                          |
|`GENERATE`|Automatic generation of nodes belonging to the element group (omissible)|
|`INPUT`   |External file name (omissible)                                          |

| Parameter Name | Parameter Value  | Contents                                                                            |
|----------------|------------------|-------------------------------------------------------------------------------------|
| `EGRP`         | &lt;egrp&gt;     | Element group name                                                                  |
| `GENERATE`     | N/A              | Automatic generation of nodes belonging to the element group                        |
| `INPUT`        | &lt;filename&gt; | External file name (omissible), can also be use together with the 2nd line or later |

2nd Line or later (when `GENERATE` is not used)

```
(2nd Line) elem1, elem2, elem3 ...
(Hereinafter the same)
```

| Parameter Name | Attributions | Contents                                      |
|----------------|--------------|-----------------------------------------------|
| `elemX`        | I            | Element number belonging to the element group |

2nd Line or later (when `GENERATE` used)

```
(2nd Line) elem1, elem2, elem3
(Hereinafter the same)
```

| Parameter Name | Attributions | Contents                                                                  |
|----------------|--------------|---------------------------------------------------------------------------|
| `elem1`        | I            | First element number in the element group                                 |
| `elem2`        | I            | Last element number in the element group                                  |
| `elem3`        | I            | Element number increment (omissible, number becomes elem3=1 when omitted) |

> Note:
>
> - Any number of elements can be inserted in one line. Any number of lines can be inserted until the next option starts.
> - It is necessary to define the element to be specified before "`!EGROUP`".
> - The element not defined in the "`!ELEMENT`" option will be excluded, and a warning message will be displayed.
> - When the specified element exists in the same group, it will be ignored and a warning message will be displayed.
> All the elements belong to the element group named "`ALL`" (generated automatically).
> One group can be defined by dividing into multiple groups.

Example of Use

```
!EGROUP, EGRP=EA01
1, 2, 3, 4, 5, 6
101, 102
205
!EGROUP, EGRP=EA02
101, 102
!EGROUP, EGRP=EA01               "501, 505" are added to group "EA01".
501, 505
!EGROUP, EGRP=EA04, GENERATE     "301, 303, 305, 307, 309, 311, 312, 313"
301, 309, 2                     are added to group "NA04".
311, 313
```

