
Absolute zero-point

1st Line

```
!ZERO
```

| Parameter |
|-----------|
| N/A       |

2nd Line or later

```
(2nd Line) ZERO
```

|Parameter Name|Attributions|Contents           |
|--------------|------------|-------------------|
|`ZERO`        |R           |Absolute zero-point|

> Note:
>
> - Omissible. Becomes "absolute zero-point = 0" when omitted.
> - When "`!ZERO`" is defined multiple times, the contents will be updated and a warning message will be displayed.

Example of Use

```
!ZERO
-273.16
```

