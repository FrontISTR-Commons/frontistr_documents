
セクションの定義

1行目

```
!SECTION, TYPE=<type>, EGRP=<egrp> [, optional parameter]
```

| パラメータ  |                                                            |
|:------------|:-----------------------------------------------------------|
|`TYPE`       |セクションタイプ（必須）                                    |
|`EGRP`       |要素グループ名（必須）                                      |
|`MATERIAL`   |ユーザー定義材料名（必須）                                  |
|`SECOPT`     |要素タイプ用補助パラメータ（省略可、省略した場合は=0となる）|
|`INPUT`      |外部ファイル名（省略可）                                    |

| パラメータ名  |パラメータ値      |内容           |
|:--------------|:-----------------|:---------------|
|`TYPE`         |`SOLID`           |ロッド，三角形，四角形，四面体，五面体，六面体要素|
|               |`SHELL`           |シェル要素                                        |
|               |`BEAM`            |梁要素                                            |
|               |`INTERFACE`       |インターフェース要素                              |
|`EGRP`         | &lt;egrp&gt;     |要素グループ名                                    |
|`MATERIAL`     | &lt;material &gt;|ユーザー定義による材料名                          |
|`SECOPT`       | &lt;secopt&gt;   | = 0：指定なし，平面応力<br/> = 1：平面ひずみ<br/> = 2：軸対称<br/> =10：0＋次数低減積分<br/> =11：1＋次数低減積分<br/> =12：2＋次数低減積分                             |
|`INPUT`        |&lt;filename&gt;  | 外部ファイル名（省略可）、2行目以降との併用も可能|

2行目以降

【TYPE=SOLID】の場合

```
（2行目）`THICKNESS`
```

| 変数名    | 属性  | 内容                           |
|:----------|:------|:-------------------------------|
|`THICKNESS`| R     |トラス要素の場合、断面積（必須）|

「`TYPE=SOLID`」の場合、「`THICKNESS`」は省略可。

　
【`TYPE=SHELL`】の場合

```
（2行目）THICKNESS, INTEGPOINTS
```

| 変数名      | 属性  | 内容                |
|:------------|:------|:--------------------|
|`THICKNESS`  | R     | シェル断面厚さ      |
|`INTEGPOINTS`| I     | シェル断面方向積分点|

【`TYPE=BEAM`】の場合

```
（2行目）vx,vy,vz,area,Iyy,Iz,Jx
```

| 変数名   | 属性  | 内容               |
|:---------|:------|:-------------------|
| vx,vy,vz | R     | 参考軸方向         |
| area     | R     | 断面面積           |
| Iyy, Izz | R     | 断面二次モーメント |
| Jx       | R     | ねじり定数         |

【`TYPE=INTERFACE`】の場合

```
（2行目）THICKNESS, GAPCON, GAPRAD1, GAPRAD2
```

| 変数名    | 属性 | 内容                               |
|:----------|:-----|:-----------------------------------|
|`THICKNESS`| R    | 断面厚さ                           |
|`GAPCON`   | R    | ギャップ熱伝達係数（省略時0）      |
|`GAPRAD1`  | R    | ギャップ輻射熱伝達係数-1（省略時0）|
|`GAPRAD2`  | R    | ギャップ輻射熱伝達係数-2（省略時0）|

注意

-   パラメータ「`TYPE`」が要素タイプと整合していない場合はエラーとなる。
-   `SECTION`情報を持たない要素がある場合はエラーとなる。
-   セクション名が重複した場合はエラーとなる。

使用例

```
!SECTION, EGRP=SOLID1, TYPE=SOLID, MATERIAL=STEEL
!SECTION, EGRP=SHELL2, TYPE=SHELL, MATERIAL=STEEL
1.0, 5
```

