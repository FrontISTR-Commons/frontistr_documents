
Definition of restricted node group

1st Line

```
!EQUATION [, optional parameter]
```

|Parameter|                              |
|---------|------------------------------|
|`INPUT`  |External file name (omissible)|


|Parameter Name|Parameter Value |Contents                            |
|--------------|----------------|------------------------------------|
|`INPUT`       |&lt;filename&gt;|External file name (omissible), can also be used together with the 2nd line or later|

2nd Line or later

```
(2nd Line) NEQ, CONST
```

```
(3rd Line or later) nod1, DOF1, A1, nod2, DOF2, A2 ... (up to seven terms for one line)
(Hereinafter repeated)
```

|Parameter Name|Attributions|Contents                                              |
|--------------|------------|------------------------------------------------------|
|`NEQ`         |I           |Number of equation terms                              |
|`CONST`       |R           |Constant term of equation (right value)               |
|`nod1`        |I/C         |1st node or node group                                |
|`DOF1`        |I           |Restricted degree of freedom of 1st node or node group|
|`A1`          |R           |Factor of 1st node or node group                      |
|`nod2`        |I/C         |2nd node or node group                                |
|`DOF2`        |I           |Restricted degree of freedom of 2nd node or node group|
|`A2`          |R           |Factor of 2nd node or node group                      |

> Note:
>
> - When a node or a node group not defined by "`!NODE`" is specified, it will be ignored and a warning message will be displayed.
> - In the case of "nod1=nod2", it will be ignored and a warning message will be displayed.
> - When a node group is specified, if the number of nodes is not consistent an error will occur.
> - The degree of freedom number differs by the type of analysis and elements. An inconsistent degree of freedom will be ignored, and a warning message will be displayed.

Example of Use

```
!EQUATION
3
101, 1, 1.0, 102, 1, -1.0, 103, 1, -1.0
2
NG1, 2, 1.0, NG5, 2, -1.0
```

