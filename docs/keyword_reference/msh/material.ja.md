
材料物性の定義

物性が温度依存している場合は対応する温度ごとにテーブル入力が可能である。

1行目

```
!MATERIAL, NAME=<name> [, optional parameter]
```

| パラメータ  |                                               |
|:------------|:----------------------------------------------|
|`NAME`       |材料名（必須）                                 |
|`ITEM`       |物性項目数（省略可、省略した場合は「1」となる）|
|`INPUT`      |外部ファイル名（省略可）                       |

| パラメータ名 | パラメータ値     | 内容                                              |
|:-------------|:-----------------|:--------------------------------------------------|
| `NAME`       | &lt;name&gt;     | 材料名                                            |
| `ITEM`       | &lt;ITEMnum&gt;  | ユーザー定義による物性項目数                      |
| `INPUT`      | &lt;filename&gt; | 外部ファイル名（省略可）、2行目以降との併用も可能 |

2行目以降

```
（2行目） !ITEM=1, SUBITEM=<k>
（3行目） VAL1-1-1, VAL1-1-2, … VAL1-1-k, TEMP1-1
（4行目） VAL1-2-1，VAL1-2-2, … VAL1-2-k, TEMP1-2
・・

（L+2行目）VAL1-L-1，VAL1-L-2, … VAL1-L-k, TEMP1-L

以下「!ITEM=<ITEMnum>」まで繰り返し定義する
```

| サブパラメータ(「`!ITEM`」に対するもの) |                                        |
|-----------------------------------------|----------------------------------------|
| `SUBITEM`                               | 各物性項目で定義されるサブ物性項目数   |
|                                         | (省略可、 省略した場合は「1」となる。) |

|サブパラメータ名 |パラメータ値      |内容                            |
|-----------------|------------------|--------------------------------|
|`SUBITEM`        |&lt;subITEMnum&gt;|ユーザー定義によるサブ物性項目数|

【m番目の物性が温度依存している場合】

温度依存のテーブルの項目数がNの場合、以下のように入力する：

```
!ITEM=m, SUBITEM=k
VALm1-1, ..., VALm1-k, TEMPm1
VALm2-1, ..., VALm2-k, TEMPm2
...
VALmN-1, ..., VALmN-k, TEMPm-N
```


| 変数名  | 属性  | 内容              |
|:--------|:------|:------------------|
| VALmn-k | R     | 物性値（温度依存）|
| TEMPmn  | R     | 対応する温度      |


TEMPm1 &lt; TEMPm2 &lt; ... &lt; TEMPmN でなければならない。

温度がTEMPm1以下の場合はVALm1、TEMPmN以上の場合はVALmNが使用される。

【m番目の物性が温度依存していない場合】

```
!ITEM=m, SUBITEM=k
VALm1-1, ..., VALm1-k
VALm2-1, ..., VALm2-k
...
VALmN-1, ..., VALmN-k
```

| 変数名  | 属性  | 内容                   |
|:--------|:------|:-----------------------|
| VALmn-k | R     | 物性値（温度依存なし） |

注意

-   材料名が重複した場合はエラーとなる。
-   「`!SECTION`」オプションで参照されている`MATERIAL`が定義されていない場合はエラーとなる。
-   「`!ELEMENT`」オプションで、パラメータ「`MATITEM`」を使用して要素ごとに物性を入力した場合の値が優先して使用される。この場合、「`!MATERIAL`」オプションを使用して入力した物性値は使用されない。
-   「`!ITEM=m`」サブオプションの数と、パラメータ「`ITEM`」の数が整合しない場合、定義されていないサブオプションがある場合はエラーとなる。
-   「`!ITEM=m`」サブオプションは、mの小さい順番に並んでいなくてもよい。
-   「`!SUBITEM=k`」サブオプション、温度依存性を使用する場合、省略した値は「0.0」となる。
-   温度依存性を使用する場合、温度の低い順に定義しなければならない。
-   温度依存性を使用する場合、同じ温度を2回以上使用した場合はエラーとなる。

使用例

```
!MATERIAL, NAME= STEEL, ITEM= 2
!ITEM=1 温度依存性なし
35.0
!ITEM=2
40.0, 0.0
45.0, 100.0
50.0, 200.0

!MATERIAL, NAME= CUPPER         項目数＝1（デフォルト値）
!ITEM=1 温度依存性なし
80.0
```

誤った使用例

例1【パラメータ「`ITEM`」と「`!ITEM=m`」サブオプションの数が整合していない-1】

```
!MATERIAL, NAME= STEEL, ITEM= 2
!ITEM=3
20.0
!ITEM=1
35.0
!ITEM= 2
40.0
```

例2【パラメータ「ITEM」と「!ITEM=m」サブオプションの数が整合していない-2】

```
!MATERIAL, NAME= STEEL, ITEM= 3
!ITEM=3
20.0
!ITEM= 2
40.0
!MATERIAL, NAME= CUPPER
...
```

弾性静解析および固有値解析

```
!MATERIAL, NAME=<name>, ITEM=<ITEMnum>
!ITEM=1, SUBITEM=2
<Young_modulus>, <Poisson_ratio>
!ITEM=2
<Density>
!ITEM=3
<Expansion_coeff>
```

|パラメータ名  |パラメータ値     |内容                                                           |
|--------------|-----------------|---------------------------------------------------------------|
| NAME         | &lt;name&gt;    | 材料名、!SECTIONのMATELIALと対応                              |
| ITEM         | &lt;ITEMnum&gt; | ユーザー定義による物性項目数（1以上）<br/>&lt;Young_modulus&gt;-----ヤング率（必須）<br/>&lt;Poisson_ratio&gt;-------ポアソン比（必須）<br/>&lt;Density&gt;--------------質量密度（ITEMnum=2のとき必須）<br/>&lt; Expansion_coeff &gt;--線膨張係数（ITEMnum=3のとき）|

（例）

```
!MATERIAL, NAME=M1,
ITEM=3                   --- 材料名M1の材料では3種の物値を定義の意
!ITEM=1, SUBITEM=2       --- !ITEM=1ではヤング率とポアソン比を定義（必須）
4000.0, 0.3
!ITEM=2                  --- !ITEM=2で質量密度を定義すること（ITEM=2のときには必須）
8.0102E-0
!ITEM=3                  --- !ITEM=3で線膨張係数を定義すること
1.0E-5
```

熱伝導解析

リンク、平面、ソリッド、3次元板要素の場合

```
!MATERIAL, NAME=<name>, ITEM=3
!ITEM=1, SUBITEM=2
<Density>, <Temperature>

!ITEM=2, SUBITEM=2
<Specific_heat>, <Temperature>

!ITEM=3, SUBITEM=2
<Conductivity>, <Temperature>
```

| パラメータ名  |パラメータ値   |内容           |
|:--------------|:--------------|:---------------|
|`NAME`         |&lt;name&gt;   |材料名、!SECTIONのMATELIALと対応         |
|`ITEM`         |&lt;ITEMnum&gt;|ユーザー定義による物性項目数（**常に3**）<br/>&lt;Density&gt;----------密度<br/>&lt;Specific_heat&gt;----比熱<br/>&lt;Conductivity&gt;-----熱伝導率<br/>&lt;Temperature&gt;------温度|

（例）

```
!MATERIAL, NAME=M1,
ITEM=3　　　             ---　材料名M1の材料では3種の物値を定義の意

!ITEM=1, SUBITEM=１　 　 ---　!ITEM=1では密度と温度を定義（必須）
7850., 300.
7790., 500.
7700., 800.

!ITEM=2, SUBITEM=1       ---　!ITEM=2では比熱と温度を定義（必須）
0.465, 300.
0.528, 500.
0.622, 800.

!ITEM=3                  ---　!ITEM=3では熱伝導率と温度を定義（必須）
43., 300.
38.6, 500.
27.7, 800.
```

インターフェース要素の場合

`!SECTION`ヘッダーで定義する。(材料データは不要)

（例）

```
!SECTION, TYPE=INTERFACE, EGRP=GAP　 ---　セクションの定義
1.0, 20.15, 8.99835E-9, 8.99835E-9
```

上記の`!SECTION`では、インターフェース要素で、グループ名=GAPに所属する要素のギャップパラメータを定義している。

* 第1パラメータ: ギャップ幅
* 第2パラメータ: ギャップ熱伝達係数
* 第3パラメータ: ギャップ輻射係数１
* 第4パラメータ: ギャップ輻射係数２

参考

```
program TEST
use hecmw
implicit REAL*8 (A-H,O-Z)
type (hecmwT_local_mesh) :: hecMESH

!C
!C    !MATERIAL, NAME=SUS304, ITEM=3
!C    !ITEM=1, SUBITEM= 3
!C      100.0, 200.0, 300.0, 0.00
!C      101.0, 210.0, 301.0, 1.00
!C      102.0, 220.0, 302.0, 2.00
!C      103.0, 230.0, 303.0, 3.00
!C    !ITEM=3, SUBITEM= 2
!C      1000.0, , 0.00
!C      1001.0, 1., 1.00
!C      1002.0, 2., 2.00
!C      1003.0, 3., 3.00
!C    !ITEM=2
!C      5000.0
!C
!C    !MATERIAL, NAME=FEC, ITEM=2
!C    !ITEM=1, SUBITEM= 3
!C      2100.0, 2200.0, 2300.0, 0.00
!C      2101.0, 2210.0, 2301.0, 1.00
!C      2102.0, 2220.0, 2302.0, 2.00
!C      2103.0, 2230.0, 2303.0, 3.00
!C      3103.0, 3230.0, 2304.0, 4.00
!C    !ITEM=2
!C      6000.0, 10.0
!C      6500.0, 30.0
!C

hecMESH%material%n_mat = 2

nn= hecMESH%material%n_mat
allocate (hecMESH%material%mat_name(nn))

hecMESH%material%mat_name(1)= 'SUS304'
hecMESH%material%mat_name(2)= 'FEC'

nn= hecMESH%material%n_mat
allocate (hecMESH%material%mat_ITEM_index(0:nn))
hecMESH%material%mat_ITEM_index(0)= 0
hecMESH%material%mat_ITEM_index(1)= 3
hecMESH%material%mat_ITEM_index(2)= hecMESH%material%mat_ITEM_index(1) + 2

hecMESH%material%n_mat_ITEM= hecMESH%material%mat_ITEM_index(hecMESH%material%n_mat)

nn= hecMESH%material%n_mat_ITEM
allocate (hecMESH%material%mat_subITEM_index(0:nn))

hecMESH%material%mat_subITEM_index(0)= 0
hecMESH%material%mat_subITEM_index(1)= 3
hecMESH%material%mat_subITEM_index(2)= hecMESH%material%mat_subITEM_index(1) + 1
hecMESH%material%mat_subITEM_index(3)= hecMESH%material%mat_subITEM_index(2) + 2
hecMESH%material%mat_subITEM_index(4)= hecMESH%material%mat_subITEM_index(3) + 3
hecMESH%material%mat_subITEM_index(5)= hecMESH%material%mat_subITEM_index(4) + 1

hecMESH%material%n_mat_subITEM=
&        hecMESH%material%mat_subITEM_index(hecMESH%material%n_mat_ITEM)

nn= hecMESH%material%n_mat_subITEM
allocate (hecMESH%material%mat_TABLE_index(0:nn))
hecMESH%material%mat_TABLE_index( 0)= 0
hecMESH%material%mat_TABLE_index( 1)= 4
hecMESH%material%mat_TABLE_index( 2)= hecMESH%material%mat_TABLE_index( 1) + 4
hecMESH%material%mat_TABLE_index( 3)= hecMESH%material%mat_TABLE_index( 2) + 4
hecMESH%material%mat_TABLE_index( 4)= hecMESH%material%mat_TABLE_index( 3) + 1
hecMESH%material%mat_TABLE_index( 5)= hecMESH%material%mat_TABLE_index( 4) + 4
hecMESH%material%mat_TABLE_index( 6)= hecMESH%material%mat_TABLE_index( 5) + 4
hecMESH%material%mat_TABLE_index( 7)= hecMESH%material%mat_TABLE_index( 6) + 5
hecMESH%material%mat_TABLE_index( 8)= hecMESH%material%mat_TABLE_index( 7) + 5
hecMESH%material%mat_TABLE_index( 9)= hecMESH%material%mat_TABLE_index( 8) + 5
hecMESH%material%mat_TABLE_index(10)= hecMESH%material%mat_TABLE_index( 9) + 2

hecMESH%material%n_mat_TABLE=
&        hecMESH%material%mat_TABLE_index(hecMESH%material%n_mat_subITEM)

nn= hecMESH%material%n_mat_TABLE
allocate (hecMESH%material%mat_VAL (nn))
allocate (hecMESH%material%mat_TEMP(nn))

hecMESH%material%mat_VAL = 0.d0
hecMESH%material%mat_TEMP= 0.d0

hecMESH%material%mat_VAL ( 1)= 100.0d0
hecMESH%material%mat_TEMP( 1)=   0.0d0
hecMESH%material%mat_VAL ( 2)= 101.0d0
hecMESH%material%mat_TEMP( 2)=   1.0d0
hecMESH%material%mat_VAL ( 3)= 102.0d0
hecMESH%material%mat_TEMP( 3)=   2.0d0
hecMESH%material%mat_VAL ( 4)= 103.0d0
hecMESH%material%mat_TEMP( 4)=   3.0d0

hecMESH%material%mat_VAL ( 5)= 200.0d0
hecMESH%material%mat_TEMP( 5)=   0.0d0

hecMESH%material%mat_VAL (13)= 5000.0d0

hecMESH%material%mat_VAL (14)= 1000.0d0
hecMESH%material%mat_TEMP (14)=   0.0d0
hecMESH%material%mat_VAL (15)= 1001.0d0
hecMESH%material%mat_TEMP (15)=   1.0d0
hecMESH%material%mat_VAL (16)= 1002.0d0
hecMESH%material%mat_TEMP (16)=   2.0d0
hecMESH%material%mat_VAL (17)= 1003.0d0
hecMESH%material%mat_TEMP (17)=   3.0d0

hecMESH%material%mat_VAL (18)=  0.0d0
hecMESH%material%mat_TEMP (18)= 0.0d0
hecMESH%material%mat_VAL (19)=  1.0d0
hecMESH%material%mat_TEMP (19)= 1.0d0
hecMESH%material%mat_VAL (20)=  2.0d0
hecMESH%material%mat_TEMP (20)= 2.0d0
hecMESH%material%mat_VAL (21)=  3.0d0
hecMESH%material%mat_TEMP (21)= 3.0d0

hecMESH%material%mat_VAL (22)= 2100.0d0
hecMESH%material%mat_TEMP (22)=   0.0d0
hecMESH%material%mat_VAL (23)= 2101.0d0
hecMESH%material%mat_TEMP (23)=   1.0d0
hecMESH%material%mat_VAL (24)= 2102.0d0
hecMESH%material%mat_TEMP (24)=   2.0d0
hecMESH%material%mat_VAL (25)= 2103.0d0
hecMESH%material%mat_TEMP (25)=   3.0d0
hecMESH%material%mat_VAL (26)= 3103.0d0
hecMESH%material%mat_TEMP (26)=   4.0d0

write (*,'(a,i10)') '%n_mat_ITEM ', hecMESH%material%n_mat_ITEM
write (*,'(a,i10)') '%n_mat_subITEM', hecMESH%material%n_mat_subITEM
write (*,'(a,i10)') '%n_mat_TABLE ', hecMESH%material%n_mat_TABLE

end program TEST
```

