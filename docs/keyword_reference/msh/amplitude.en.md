
Specifies the changes of time of the variables which provide the load conditions in the step.

```
!AMPLITUDE, NAME=<name> [, optional parameter]
VAL1, T1, VAL2, T2, VAL3, T3 ...

(up to four items in one line)
```

|Parameter   |                              |
|------------|------------------------------|
|`NAME`      |Name (mandatory)              |
|`DEFINITION`|Type (omissible)              |
|`TIME`      |Type of time (omissible)      |
|`VALUE`     |Type of value (omissible)     |
|`INPUT`     |External file name (omissible)|

|Parameter Name|Parameter Value  |Contents                                 |
|--------------|-----------------|-----------------------------------------|
|`NAME`        |&lt;name&gt;     |`AMPLITUDE` Name                         |
|`DEFINITION`  |`TABULAR`        |Default (default only in current version)|
|`TIME`        |`STEP TIME`      |Default (default only in current version)|
|`VALUE`       |`RELATIVE`       |Relative value (default)                 |
|              |`ABSOLUTE`       |Absolute value                           |
|`INPUT`       | &lt;filename&gt;|External file name (omissible), can also be use together with the 2nd line or later|


|Parameter Name |Attributions|Contents        |
|---------------|------------|----------------|
|`VAL1`         |R           |Value at time T1|
|`T1`           |R           |Time T1         |
|`VAL2`         |R           |Value at time T2|
|`T2`           |R           |Time T2         |
|`VAL3`         |R           |Value at time T3|
|`T3`           |R           |Time T3         |

