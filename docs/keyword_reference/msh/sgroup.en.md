
Definition of surface group

1st Line

```
!SGROUP, SGRP=<sgrp> [, optional parameter]
```

|Parameter|                              |
|---------|------------------------------|
|`SGRP`   |Surface group name (mandatory)|
|`INPUT`  |External file name (omissible)|

|Parameter Name|Parameter Value |Contents                                                                            |
|--------------|----------------|------------------------------------------------------------------------------------|
|`SGRP`        |&lt;sgrp&gt;    |Surface group name                                                                  |
|`INPUT`       |&lt;filename&gt;|External file name (omissible), can also be used together with the 2nd line or later|

2nd Line or later

```
(2nd Line) elem1, lsuf1, elem2, lsuf2, elem3, lsuf3, ...
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents                                                          |
|--------------|------------|------------------------------------------------------------------|
|elemX         |I           |Element number belonging to the surface group                     |
|lsufX         |I           |Local surface number of the element belonging to the surface group|

> Note:
>
> - For the element type and surface number, refer to "Chapter 4 Element Library".
> - The surface consists of a combination of (elements and local surface numbers). Any number of surfaces can be inserted in one line. Any number of lines can be inserted until the next option starts. The combination of (elements and local surface numbers) must be in the same line.
> - It is necessary to define the element to be specified before "`!SGROUP`".
> - The element not defined in "`!ELEMENT`" option will be excluded, and a warning message will be displayed.
> - The surface which includes the element not defined in "`!ELEMENT`" option will be excluded, and a warning message will be displayed.
> - The surface where the element type and the surface number are not consistent will be excluded, and a warning message will be displayed.
> - One group can be defined by dividing into multiple groups.

Example of Use

```
!SGROUP, SGRP= SUF01
101, 1, 102, 1, 103, 2, 104, 2
201, 1, 202, 1
501, 1
!SGROUP, SGRP= SUF02
101, 2, 102, 2
!SGROUP, SGRP= EA01     "(601,1) and (602 2)" are added to group "SUF01".
601, 1
602, 2
```

Incorrect Example of Use

- Example 1: [When (elements, and local surface numbers) group exists in multiple lines]

```
!SGROUP, SGRP= SUF01
101, 1, 102, 1, 103
1, 104, 1
```

- Example 2: [Local surface numbers and element type are not consistent]

```
!ELEMENT, TYPE= 211, SECTION= A
101, 1, 2, 3
102, 2, 3, 4
...
!SGROUP, SGRP= SUF01
101, 1
101, 2
101, 4                  Since a 4th surface does not exist in a triangular element,
this combination will be disregarded.
```

