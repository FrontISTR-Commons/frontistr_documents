
Definition of Embedded Pairs

1行目

```
!EMBED PAIR, NAME=<name>
```

|Parameter|                             |
|:------------|:-----------------|
|`NAME`   |Embed pair name (mandatory)|

|Parameter Name|Attributions|Contents                                |
|:--------------|:-------------|:---------------|
|`NAME`         |&lt;name&gt;  |Embed pair name  |

2nd Line or later

```
(2nd Line or later) SLAVE_NGRP, MASTER_EGRP
(Hereinafter the same)
```

|Parameter Name|Attributions|Contents                                |
|:-------------|:------|:-----------------------------|
| `SLAVE_GRP`  | C     | Slave node group name |
| `MASTER_GRP` | C     | Element group name of the master volume  |


