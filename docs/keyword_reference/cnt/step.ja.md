##### `!STEP` (2-11)

解析ステップの設定

非線形静解析、非線形動解析では必須

上記以外の解析でこの定義を省略すると、すべての境界条件が有効になり、1ステップで計算

材料特性が粘弾性およびクリープの場合、`TYPE=VISCO`を指定し、計算時間条件を設定

###### パラメータ

```
TYPE     = STATIC(default値) / VISCO (準静的解析)
SUBSTEPS = 境界条件の分割ステップ数(デフォルト: 1)
CONVERG  = 収束判定閾値(デフォルト: 1.0e-6)
MAXITER  = 非線形解析における最大反復計算回数(デフォルト: 50)
AMP      = 時間関数名(!AMPLITUDEで指定)
INC_TYPE = FIXED（固定増分・default値） / AUTO（自動増分）
MAXRES   = 最大許容残差の設定（デフォルト：1.0e+10）
TIMEPOINTS = 時刻リスト名（!TIME_POINTS, NAMEで指定）
AUTOINCPARAM = 自動増分パラメータセット名（!AUTOINC_PARAM, NAMEで指定）
MAXCONTITER = 接触解析における最大接触反復回数（デフォルト：10）
```

** 2行目 **

INC_TYPE=FIXEDの場合（TYPE=STATICの場合は省略可）

```
(2行目) DTIME, ETIME
```

| 変数名 | 属性 | 内容 |
| :-- | :-- | :-- |
|DTIME|R|時間増分（デフォルト：1/SUBSTEPS）|
|ETIME|R|ステップ時間幅（デフォルト：1）|

INC_TYPE=AUTOの場合（TYPEによらず指定）

```
(2行目) DTIME_INIT, ETIME, MINDT, MAXDT
```

| 変数名 | 属性 | 内容 |
| :-- | :-- | :-- |
|DTIME_INIT|R|初期時間増分|
|ETIME|R|ステップ時間幅|
|MINDT|R|時間増分下限|
|MAXDT|R|時間増分上限|

** 3行目以降 **

```
  BOUNDARY, id                                  id=!BOUNDARYで定義したGRPID
  LOAD, id                                      id=!CLOAD, !DLOAD, !SPRING, !TEMPERATUREで定義したGRPID
  CONTACT, id                                   id=!CONTACTで定義したGRPID
```

###### 使用例
固定時間増分の使用例

```
!STEP, CONVERG=1.E-8
  0.1, 1.0
  BOUNDARY, 1
  LOAD, 1
  CONTACT, 1
```

自動増分調整を有効にし、初期時間増分0.01、ステップ時間幅2.5、時間増分下限1E-5、時間増分上限0.3、最大サブステップ数を200に設定する

```
    !STEP, INC_TYPE=AUTO, SUBSTEPS=200
    0.01, 2.5, 1E-5, 0.3
```

自動増分調整を有効にし、時刻リストTP1を計算・結果出力時刻として指定する

```
    !STEP, INC_TYPE=AUTO, TIMEPOINTS=TP1
    0.1, 2.0, 1E-3, 0.2
```

###### 備考

- 自動増分調整の場合、SUBSTEPSは最大サブステップ数として扱われる
- 時刻リスト名TIMEPOINTSおよび自動接触パラメータセット名AUTOINCPARAMの指定はINC_TYPE=AUTOのときのみ有効
- TIMEPOINTSを指定する場合、指定先の!TIME_POINTは!STEPカードより前に定義されていなければならない。
- AUTOINCPARAMを指定する場合、指定先の!AUTOINC_PARAMは!STEPカードより前に定義されていなければならない。また、本パラメータ省略時はデフォルトの自動増分パラメータセットが使用される
