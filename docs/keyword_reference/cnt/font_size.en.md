##### `!font_size`, `!font_color`, `!backgroud_color` (P2-19, P2-20, P2-21)

Specifies the background color and character font.

![Example of Background and Font Setting](media/analysis05_17.png){.center width="80%"}

**Figure 7.4.15: Example of Background and Font Setting**

