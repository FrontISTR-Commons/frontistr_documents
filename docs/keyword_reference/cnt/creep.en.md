##### `!CREEP` (2-2-5)

Definition of creep material

`!CREEP` must be defined together with `!ELASTIC`.

###### Parameter

```
TYPE = NORTON (Default)
DEPENDENCIES = 0 (Default) / 1
```

** 2nd Line or later **

```
(2nd line) A, n, m, Tempearature
```

|Parameter Name|Attributions |Contents     |
|-------------|------|----------|
| A           | R    |Material modulus|
| n           | R    |Material modulus|
| m           | R    |Material modulus|
| Tempearture | R    |Temperature(required when `DEPENDENCIES=1`) |

