##### `!STEP` (2-11)

Analysis step settings

Required for nonlinear static and nonlinear dynamic analysis

If you omit this definition for any analysis other than the above, all boundary conditions are in effect and the calculation is done in one step

If the material properties are visco-elasticity and creep, specify `TYPE=VISCO` and set the calculation time condition to

###### Parameter

```
TYPE     = STATIC (default)/VISCO (quasi-static analysis)
SUBSTEPS = Number of substeps of the boundary conditions (Default: 1)
CONVERG  = Convergence threshold (Default: 1.0e-6)
MAXITER  = Maximum number of iterations in nonelinear analysis (Default: 50)
AMP      = Time function name (specified in !AMPLITUDE)
INC_TYPE = FIXED (fixed increment, default) / AUTO (automatic increment)
MAXRES   = Setting of maximum allowable residuals (default: 1.0e+10)
TIMEPOINTS = Name of the time list (specified by `!TIME_POINTS, NAME`)
AUTOINCPARAM = auto-incremental parameter set name (specified by `!AUTOINC_PARAM, NAME`)
MAXCONTITER = Maximum number of contact iterations in contact analysis (default: 10)
```

** 2nd line or later **

In case of `INC_TYPE=FIXED` (If `TYPE=STATIC`, it can be omitted.)

```
(2nd line) DTIME, ETIME
```

|Parameter Name|Attribution|Contents|
|--------|------|-----------------------------------------|
| DTIME  | R    |Time increment value (Default: 1/SUBSTEPS)|
| ETIME  | R    |End value of time increment in this step (Default: 1)|

In case of `INC_TYPE=AUTO` (regardless of `TYPE`)

```
(2nd line) DTIME_INIT, ETIME, MINDT, MAXDT
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|----------------------------------|
| DTIME_INIT   | R         | Initial time increment           |
| ETIME        | R         | Step time width                  |
| MINDT        | R         | Lower limit of time increments   |
| MAXDT        | R         | Maximum limit of time increments |

** 3rd line or later **

```
BOUNDARY, id          GRPID defined in id=!BOUNDARY
LOAD, id              GRPID defined in id=!CLOAD, !DLOAD, !SPRING, !TEMPERATURE
CONTACT, id           GRPID defined in id=!CONTACT
```

###### example

####### Examples of fixed time increment usage

```
!STEP, CONVERG=1.E-8
0.1, 1.0
BOUNDARY, 1
LOAD, 1
CONTACT, 1
```

Enable automatic incremental adjustment, set the initial time increment to 0.01, step time width to 2.5, lower time increment 1E-5, upper time increment 0.3, and maximum number of sub-steps to 200.

```
!STEP, INC_TYPE=AUTO, SUBSTEPS=200
0.01, 2.5, 1E-5, 0.3
```

Enable automatic incremental adjustment and specify time list TP1 as the calculated and resulting output time

```
!STEP, INC_TYPE=AUTO, TIMEPOINTS=TP1
0.1, 2.0, 1E-3, 0.2
```

####### Note

- In the case of automatic incremental adjustment, `SUBSTEPS` is treated as the maximum number of substeps
- Time-list name `TIMEPOINTS` and automatic contact parameter set `AUTOINCPARAM` are valid only when `INC_TYPE=AUTO`.
- if `TIMEPOINTS` is specified, the destination `!TIME_POINT` must be defined before the `!STEP` card.
- if `AUTOINCPARAM` is specified, the destination `!AUTOINC_PARAM` must be defined prior to the `!STEP` card. If this parameter is omitted, the default auto-incremental parameter set is used.

