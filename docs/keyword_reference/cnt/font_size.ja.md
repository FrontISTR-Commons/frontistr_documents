##### `!font_size`, `!font_color`, `!backgroud_color` (P2-19, P2-20, P2-21)

背景色や文字フォントを指定する。

![backgroundとfontの設定例](media/analysis05_17.png){.center width="80%"}

図7.4.15 `background`と`font`の設定例
