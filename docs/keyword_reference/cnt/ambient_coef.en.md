##### `!ambient_coef`, `!diffuse_coef`, `!specular_coef` (P2-8, P2-9, P2-10)

Coefficient setting of lighting model

When the ambient_coef is increased, information on the 3D depth direction is impaired.

![Example of Lighting Model Parameter Setting](media/analysis05_15.png){.center width="80%"}

