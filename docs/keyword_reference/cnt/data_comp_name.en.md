##### `!data_comp_name`, `!data_comp`, `!data_subcomp` (P3-1, P3-3, P3-4)

Specifies the physical values of the isosurface to be visualized when `surface_style=2`.

![Example of data_comp, data_subcomp and data_comp_name Setting](media/analysis05_18.png){.center width="80%"}

**Figure 7.4.16: Example of data_comp, data_subcomp and data_comp_name Setting**

