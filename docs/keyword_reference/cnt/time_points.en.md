##### `!TIME_POINTS` (2-13)

###### Parameter

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| NAME         | C         | Time list name (required) |
| TIME         | C         | STEP (input based on the time from the step start time, default value) / TOTAL (input based on total time from the initial time) |
| GENERATE     | -         | Automatic generationi of time points by start time, end time and time interval |


** 2nd line or later **

When don't use `GENERATE`

```
(2nd line or later) TIME
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| TIME         | R         | time   |

When using `GENERATE`

```
(2nd line) STIME, ETIME, INTERVAL
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| STIME        | R         | start time |
| ETIME        | R         | end time   |
| INTERVAL     | R         | interval between time points |

###### example

Time 1.5, 2.7 and 3.9 are defined as total times without using `GENERATE`.

```
!TIME_POINTS, TIME=STEP, GENERATE, NAME=TP1
1.5, 3.9, 1.2
```

###### note

- The time points must be entered in ascending order.


