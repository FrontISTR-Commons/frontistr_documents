##### `!method` (P4-1)

When specifying the surfaces and cut end, specifies the setting method of the surface.

```
!surface_num =2
!surface
!surface_style=3
!method=5
!coef=0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, -0.35
!color_comp_name = temperature
```
**Figure 7.4.17: Example of Setting Method**

Accordingly, the cut end of the plane surface z = 0.35 and z = -0.35 will be visualized.
