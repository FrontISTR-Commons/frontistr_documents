#### `!VISUAL` (P1-0）

可視化手法を指定する。

```
METHOD = PSR             : サーフェスレンダリング
  visual_start_step      : 可視化処理を始めるタイムステップ番号の指定  (デフォルト: 1)
  visual_end_step        : 可視化処理を終了するタイムステップ番号の指定(デフォルト: すべて)
  visual_interval_step   : 可視化処理を行うタイムステップ間隔の指定    (デフォルト: 1)
```
