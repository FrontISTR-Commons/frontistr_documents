##### `!ELASTIC` (2-2-1)

Definition of elastic material

###### Parameter

```
TYPE = ISOTROPIC (Default)/ ORTHOTROPIC / USER
DEPENDENCIES = 0 (Default)/1
INFINITESIMAL    When specified, infinitesimal deformation is assumed
```

###### 2nd Line or later

- In the case of `TYPE = ISOTROPIC`

```
(2nd Line) YOUNGS, POISSION, Temperature
```

|Parameter Name|Attributions |Contents |
|--------------|-----|--------------------------------------------|
|YOUNGS        |R    |Young's Modulus                             |
|POISSON       |R    |Poisson's Ratio                             |
|Temperature   |R    |Temperature (required when DEPENDENCIES = 1)|

- In case of `TYPE=ORTHOTROPIC`

(2nd Line) E1, E2, E3, &nu;12, &nu;13, &nu;23, G12, G13, G23, Temperature

\begin{equation}
\begin{bmatrix}
\varepsilon\_{11} \\\
\varepsilon\_{22} \\\
\varepsilon\_{33} \\\
2\varepsilon\_{12} \\\
2\varepsilon\_{23} \\\
2\varepsilon\_{31}
\end{bmatrix}
=
\begin{bmatrix}
1/E_1 & -\nu\_{12}/E_1 & -\nu\_{13}/E_1 & 0         & 0         & 0 \\\
& 1/E_2          & -\nu\_{23}/E_2 & 0         & 0         & 0 \\\
&                & 1/E_3          & 0         & 0         & 0 \\\
&                &                & 1/G\_{12} & 0         & 0 \\\
& Symmetric      &                &           & 1/G\_{23} & 0 \\\
&                &                &           &           & 1/G\_{31}
\end{bmatrix}
\begin{bmatrix}
\sigma\_{11} \\\
\sigma\_{22} \\\
\sigma\_{33} \\\
\sigma\_{12} \\\
\sigma\_{23} \\\
\sigma\_{31}
\end{bmatrix}
\end{equation}

- In the case of `TYPE=USER`

```
(2nd line - 10th line)v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```

