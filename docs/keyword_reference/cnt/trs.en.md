##### `!TRS` (2-2-8)

Thermorheological Simplicity description on temperature behavior of viscoelastic materials

This definition must be placed after `!VISCOELASTIC`. If not, this definition will be ignored.

###### Parameter

```
DEFINITION = WLF(Default) /ARRHENIUS
```

** 2nd line or later **

(2nd line) $\theta_0$, C<sub>1</sub>, C<sub>2</sub>

| Parameter Name               | Attributions | Contents    |
|------------------------------|--------------|-----------------------|
| $\theta_0$                   | R            | Reference temperature |
| C<sub>1</sub>, C<sub>2</sub> | R            | Material constants    |

