##### `!DFLUX` (4-4)

Definition of distributed heat flux and internal heat generation given to surface of element

###### Parameter

```
AMP = Flux history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) ELEMENT_GRP_NAME, LOAD_type, Value
```

|Paramater Name|Attributions|Contents|
|------------------|------|------------------------------|
| ELEMENT_GRP_NAME | C/I  |Element group name or element ID|
| LOAD_type        | C    |Load type No.|
| Value            | R    |Heat flux value|

###### Parameter

```
!DFLUX
ALL, S1, 1.0
!DFLUX, AMP=FLUX2
ALL, S0, 1.0
```

####### Load Parameters

|Load Type No.|Applied Surface|Parameter|
|----------------|----------|------------|
| BF             | Element overall|Calorific value|
| S1             | Surface No. 1|Heat flux value|
| S3             | Surface No. 2|Heat flux value|
| S4             | Surface No. 3|Heat flux value|
| S5             | Surface No. 4|Heat flux value|
| S6             | Surface No. 5|Heat flux value|
| S2             | Surface No. 6|Heat flux value|
| S3             | Shell surface|Heat flux value|

