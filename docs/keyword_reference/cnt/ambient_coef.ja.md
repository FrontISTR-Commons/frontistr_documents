##### `!ambient_coef`, `!diffuse_coef`, `!specular_coef` (P2-8, P2-9, P2-10)

照明モデルの係数設定

`ambient_coef`を増加すると3次元の奥行き方向の情報が損なわれる。

![照明モデルパラメータの設定例](media/analysis05_14.png){.center width="80%"}

図7.4.13 照明モデルパラメータの設定例
