##### `!VERSION` (1-1)

Specifies the solver version number. The current version number is 5.

###### Example of Use

```
!VERSION
5
```

