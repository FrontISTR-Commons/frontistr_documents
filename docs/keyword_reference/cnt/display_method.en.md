##### `!display_method` (P1-4)

Display method (Default: 1)

1. Color code display
2. Boundary line display
3. Color code and boundary line display
4. Display of 1 specified color
5. Isopleth line display by classification of color

![Example of display_method Setting](media/analysis05_06.png){.center width="80%"}

**Figure 7.4.4:Example of display_method Setting**

