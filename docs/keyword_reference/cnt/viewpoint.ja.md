##### `!viewpoint`, `!look_at_point`, `!up_direction` (P2-5, P2-6, P2-7)

###### `viewpoint`: 視点の位置を座標で指定する。

```
省略値
x = (xmin + xmax)/2.0,
y = ymin + 1.5 *(ymax – ymin),
z = zmin + 1.5 *(zmax – zmin)
```

###### `look_at_point`: 視線の位置を指定する。

```
(省略値: データの中心)

up_direction: Viewpoint, look_at_point とup_direction にてビューフレームを指定する。
              default : 0.0 0.0 1.0

View coordinate frame:
              原点    : look_at_point
              z軸     : viewpoint - look_at_point
              x軸     : up × z axis
              y軸     : z axis × x axis
```

![ビューフレームの決定法](media/analysis05_15.png){.center width="50%"}

図7.4.11　ビューフレームの決定法

![!viewpoint, !look_at_pointとup_directionの設定例](media/analysis05_13.png){.center width="80%"}

図7.4.12 `!viewpoint`, `!look_at_point`と`up_direction`の設定例
