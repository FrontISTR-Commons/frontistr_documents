##### `!isoline_number`, `!isoline_color` (P1-9, P2-22)

When `display_method=2`,`3` or `5`

![Example of isoline_number and isoline_color Setting](media/analysis05_08.png){.center width="80%"}

**Figure 7.4.6: Example of isoline_number and isoline_color Setting**

