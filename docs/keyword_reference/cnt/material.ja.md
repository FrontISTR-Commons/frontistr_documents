##### `!MATERIAL`　(2-2)

###### 材料物性の定義

材料物性の定義は`!MATERIAL`と以降に置く`!ELASTIC`、`!PLASTIC`などとセットで使用する。`!MATERIAL`の前に置く`!ELASTIC`、`!PLASTIC`などは無視される。

注: 解析制御データで`!MATERIAL`を定義すると、メッシュデータ内の`!MATERIAL`定義は無視される。解析制御データで`!MATERIAL`を定義しない場合は、メッシュデータ内の`!MATERIAL`定義が用いられる。

###### パラメータ

```
NAME = 材料名
```
