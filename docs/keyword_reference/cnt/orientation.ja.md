##### `!ORIENTATION` (1-10)

局所座標系を定義する。

###### パラメータ

```
NAME = 局所座標系名
DEFINITION = COORDINATES (Default値)/NODES
```

####### `DEFINITION = COORDINATES`の場合

(2行目) a1, a2, a3, b1, b2, b3, c1, c2, c3

| 変数名        | 属性      | 内容          |
|---------------|-----------|---------------|
| a1, a2, a3    | R         | a点の全体座標 |
| b1, b2, b3    | R         | b点の全体座標 |
| c1, c2, c3    | R         | c点の全体座標 |

####### `DEFINITION= NODES`の場合

(2行目) a, b, c

| 変数名     | 属性  | 内容   |
|------------|-------|--------|
| a, b, c    | I     |節点番号|

![Orientation](media/analysis05_02.png){.center width="50%"}
