#### `!surface_num`, `!surface`, `!surface_style` (P1-1～3）

##### `!surface_num` (P1-1)

1つのサーフェスレンダリング内のサーフェス数

例: 図7.4.1は4つのサーフェスがあり、2つは等値面でpressure=1000.0 とpressure=-1000.0、 2つは平面の切り口で z= -1.0 とz= 1.0である。

![surface_numの設定例](media/analysis05_03.png){.center width="50%"}
図7.4.1 `surface_num`の設定例

##### `!surface` (P1-2)

サーフェスの内容を設定する。

例： 図7.4.2は4つのサーフェスがありその内容は以下の通りである。

![surfaceの設定例](media/analysis05_04.png){.center width="50%"}

図7.4.2 `surface`の設定例

```
!surface_num = 2
!SURFACE
!surface_style = 2
!data_comp_name = press
!iso_value = 1000.0
!display_method = 4
!specified_color = 0.45
!output_type = BMP
!SURFACE
!surface_style = 2
!data_comp_name = press
!iso_value = -1000.0
!display_method = 4
!specified_color = 0.67
```

##### `!surface_style` (P1-3)

サーフェスのスタイルを指定する。

  1. 境界面
  2. 等値面
  3. 任意の2次曲面<br/>coef[1]x2 + coef[2]y2 + coef[3]z2 + coef[4]xy + coef[5]xz<br/>+ coef[6]yz + coef[7]x + coef[8]y + coef[9]z + coef[10]=0

![surface/styleの設定例](media/analysis05_05.png){.center width="80%"}

図7.4.3　surface/styleの設定例
