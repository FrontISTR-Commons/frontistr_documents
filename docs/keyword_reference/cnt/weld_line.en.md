##### `!WELD_LINE` (4-10)

Definition of weld line (Linear)

###### Parameter

N/A

** 2nd line **

```
(2nd line) I, U, Coef, V
```

|Parameter Name |Attributions |Contents|
|--------|------|----------------------|
| I      | R    |Current|
| U      | R    |Voltage|
| Coef   | R    |Heat input coefficient|
| V      | R    |Movement speed of the welding torch|

** 3rd line **

```
(3rd line) EGROUP, XYZ, C1, C2, H, tstart
```

|Parameter Name |Attributions |Contents|
|--------|------|----------------------------------------------|
| EGROUP | C    |Element group name for heat input|
| XYZ    | I    |Movement direction of welding torch (Degree of freedom No.)|
| C1     | R    |Starting point coordinates of welding torch|
| C2     | R    |Ending point coordinates of welding torch|
| H      | R    |Width of welding torch, inside which thermo energy inputted|
| tstart | R    |Welding start time|

#### Control Data for Dynamic Analysis

