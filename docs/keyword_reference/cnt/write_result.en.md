##### `!WRITE, RESULT` (1-4)

Specifies the output of the analysis results file.

###### Parameter

```
FREQUENCY = step interval of output (Default:1)
```

###### Example of Use

```
!WRITE, RESULT, FREQUENCY=2
```

