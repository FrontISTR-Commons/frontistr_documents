##### `!EIGEN` (3-1)

固有値解析のパラメータ設定

###### パラメータ

なし

** 2行目以降 **

```
(2行目) NGET, LCZTOL, LCZMAX
```

| 変数名  | 属性 | 内容                        |
|---------|------|-----------------------------|
| NSET    | I    | 固有値数                    |
| LCZTOL  | R    | 許容差 (デフォルト: 1.0e-8) |
| LCZMAX  | I    | 最大反復数 (デフォルト: 60) |

###### 使用例

```
!EIGEN
  3, 1.0e-10, 40
```
