##### `!HEAT` (4-1)

Definition of control data regarding calculation

###### Parameter

```
TIMEPOINTS = Time list name (specify with !TIME_POINTS, NAME)
```

** 2nd line or later **

```
(2nd line) DT, ETIME, DTMIN, DELTMX, ITMAX, ESP
```

|Parameter Name|Attributions|Contents|
|--------|------|-------------------------------------------------------------|
| DT     | R    | Initial time increment<br/>&#8806; 0: Steady calculation<br/>&gt; 0: Unsteady calculation|
| ETIME  | R    | Unsteady calculation time (mandatory for unsteady calculation)|
| DTMIN  | R    | Minimum time increment<br/>&#8806; 0: Fixed time increment<br/>&gt; 0: Auto time increment |
| DELTMX | R    | Allowable change in temperature|
| ITMAX  | I    | Maximum number of iterations of nonlinear calculation (Default: 20)|
| EPS    | R    | Convergence judgment value (Default: 1.0e-6)

###### Example of Use

```
!HEAT
(No data)               ----- Steady calculation
!HEAT
0.0                     ----- Steady calculation
!HEAT
10.0, 3600.0            ----- Fixed time increment unsteady calculation
!HEAT
10.0, 3600.0, 1.0       ----- Auto time increment unsteady calculation
!HEAT
10.0, 3600.0, 1.0, 20.0 ----- Auto time increment unsteady calculation
```

###### Remarks

Only when performing auto time increment unsteady calculation,
TIMEPOINTS parameter can be used to specify time points at which
results and/or visualization files are output.


