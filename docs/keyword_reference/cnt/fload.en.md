#### `!FLOAD` (5-6)

Defining external forces applied in frequency response analysis

###### Parameter

```
LOAD CASE = (1: Real part, 2: Imaginary part)
```
** 2nd line or later **

```
(2nd line) NODE_ID, DOF_id, Value
```

|Parameter Name|Attributions|Contents|
|---------|------|------------------------------------------------|
| NODE_ID | I/C  |Node ID, node group name or surface group name|
| DOF_id  | I    |Degree of freedom No.|
| Value   | R    |Load value|

###### Example of Use

```
!FLOAD, LOAD CASE=2
_PickedSet5, 2, 1.0
```

#### Solver Control Data

