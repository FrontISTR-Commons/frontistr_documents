##### `!SFLUX` (4-5)

Definition of distributed heat flux by surface group

###### Parameter

```
AMP = Flux history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) SURFACE_GRP_NAME, Value
```

|Parameter Name| Attributions |Contents|
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | Surface group name |
| Value            | R    | Heat flux value |

###### Example of Use

```
!SFLUX
SURF, 1.0
!SFLUX, AMP=FLUX3
SURF, 1.0
```

