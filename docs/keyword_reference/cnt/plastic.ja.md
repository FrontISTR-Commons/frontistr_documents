##### `!PLASTIC` (2-2-2)

塑性材料の定義

`!PLASTIC`を定義するときは、同じ`!MATERIAL`の中で`!ELASTIC`も同時に定義しなければならない。

###### パラメータ

```
YIELD        = MISES (Default値)、Mohr-Coulomb、DRUCKER-PRAGER、USER
HARDEN       = BILINEAR (Default値)、MULTILINEAR、SWIFT、RAMBERG-OSGOOD、KINEMATIC、COMBINED
DEPENDENCIES = 0 (Default値) / 1
INFINITESIMAL    指定した場合、微小変形の前提が適用される
```

** 2行目以降 **

####### `YIELD = MISES`の場合 (Default値)

** `HARDEN = BILINEAR` (Default値)の場合 **

```
(2行目) YIELD0, H
```

** `HARDEN = MULTILINEAR`の場合 **

```
(2行目) YIELD, PSTRAIN, Temperature
(3行目) YIELD, PSTRAIN, Temperature

...続く
```

** `HARDEN = SWIFT`の場合 **

```
(2行目) ε0, K, n
```

** `HARDEN = RAMBERG-OSGOOD`の場合 **

```
(2行目) ε0, D, n
```

** `HARDEN = KINEMATIC`の場合 **

```
(2行目) YIELD0, C
```

** `HARDEN = COMBINED`の場合 **

```
(2行目) YIELD0, H, C
```

####### `YIELD =` Mohr-Coulomb または Drucker-Pragerの場合

** `HARDEN = BILINEAR`(Default値)の場合 **

```
(2行目) c, $\phi$, H, $\psi$
```

** `HARDEN = MULTILINEAR`の場合 **

```
(2行目) $\phi$, $\psi$
(3行目) c, PSTRAIN
(4行目) c, PSTRAIN
...続く
```
`HARDEN = `他は無視され、Default値(`BILINEAR`)になる。

| 変数名     | 属性 | 内容                |
|------------|------|---------------------|
| YIELD0 　  | R    | 初期降伏応力        |
| H          | R    | 硬化係数            |
| PSTRAIN 　 | R    | 塑性ひずみ          |
| YIELD      | R    | 降伏応力            |
| \(\varepsilon0, K, n\)   | R    |\(\overline{\sigma} = k\left( \varepsilon_{0} + \overline{\varepsilon} \right)^{n}\)|
| \(\varepsilon0, D, n\)   | R    |\(\varepsilon = \frac{\sigma}{E} + \varepsilon_{0}\left( \frac{\sigma}{D} \right)^{n}\)|
| $\phi$     | R    | 内部摩擦角          |
| $\psi$     | R    | ダイレイタンシー角 (デフォルト: $\phi$と同じ値)|
| c          | R    | 粘着力              |
| C          | R    | 線形移動硬化係数    |
| Tempearture|R     | 温度(DEPENDENCIES=1の時に必要) |
| v1, v2...v10 | R  | 材料定数            |

** YIELD= USERの場合 **

```
(2行目以降) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```

###### 使用例

```
!PLASTIC, YIELD=MISES, HARDEN=MULTILINEAR, DEPENDENCIES=1
  276.0, 0.0, 20.
  296.0, 0.0018, 20.
  299.0, 0.0053, 20.
  303.0, 0.008, 20.
  338.0, 0.0173, 20.
  372.0, 0.0271, 20.
  400.0, 0.037, 20.
  419.0, 0.0471, 20.
  437.0, 0.0571, 20.
  450.0, 0.0669, 20.
  460.0, 0.0767, 20.
  469.0, 0.0867, 20.
  477.0, 0.0967, 20.
  276.0, 0.0, 100.
  276.0, 0.0018, 100.
  282.0, 0.0053, 100.
  295.0, 0.008, 100.
  330.0, 0.0173, 100.
  370.0, 0.0271, 100.
  392.0, 0.037, 100.
  410.0, 0.0471, 100.
  425.0, 0.0571, 100.
  445.0, 0.0669, 100.
  450.0, 0.0767, 100.
  460.0, 0.0867, 100.
  471.0, 0.0967, 100.
  128.0, 0.0, 400.
  208.0, 0.0018, 400.
  243.0, 0.0053, 400.
  259.0, 0.008, 400.
  309.0, 0.0173, 400.
  340.0, 0.0271, 400.
  366.0, 0.037, 400.
  382.0, 0.0471, 400.
  396.0, 0.0571, 400.
  409.0, 0.0669, 400.
  417.0, 0.0767, 400.
  423.0, 0.0867, 400.
  429.0, 0.0967, 400.
```

指定の温度また塑性ひずみに関する上記の入力データから内挿して、加工硬化係数を計算することになる。各温度値に対して、同じ`PSTRAIN`配列を入力することが必要になる。
