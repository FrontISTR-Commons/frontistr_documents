#### `!ACCELERATION` (5-3)

Definition of acceleration boundary conditions

###### Parameter

```
TYPE = INITIAL (Initial acceleration boundary conditions)
= TRANSIT ((Time history acceleration boundary conditions
specified in AMPLITUDE; Default)
AMP  = Time function name (specified in !AMPLITUDE)
Provides the relationship between time t and factor f(t) in !AMPLITUDE.
The time multiplied by factor f(t) to the following Value
becomes the restrained value of that time (when not specified:
time and factor relationship becomes f(t) = 1.0).
```

** 2nd line or later **

```
(2nd line) NODE_ID, DOF_idS, DOF_idE, Value
```

|Parameter Name|Attributions|Contents|
|---------|------|------------------------------|
| NODE_ID | I/C  |Node ID or node group name|
| DOF_idS | I    |Start No. of restricted degree of freedom|
| DOF_idE | I    |End No. of restricted degree of freedom|
| Value   | R    |Restricted value (Default: 0)|

###### Example of Use

```
!ACCELERATION, TYPE=TRANSIT, AMP=AMP1
1, 1, 3, 0.0
ALL, 3, 3
i* Restricted value is 0.0
!ACCELERATION, TYPE=INITIAL
1, 3, 3, 1.0
2, 3, 3, 1.0
3, 3, 3, 1.0
```

Note: The acceleration boundary conditions are different than the displacement boundary conditions, and the multiple degrees of freedom can not be defined collectively. Therefore, the same number must be used for DOF_idS and DOF_idE.

When the `TYPE` is `INITIAL`, `AMP` becomes invalid.

