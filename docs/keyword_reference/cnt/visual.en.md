#### `!VISUAL` (P1-0)

Specifies the visualization method.

```
METHOD = PSR             : Surface rendering
visual_start_step      : Specification of time step number which starts the visualization process
(Default: 1)
visual_end_step        : Specification of time step number which ends the visualization process
(Default: All)
visual_interval_step   : Specification of time step interval which performs the visualization process
(Default: 1)
```

