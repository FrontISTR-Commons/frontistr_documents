##### `!OUTPUT_SSTYPE` (1-13)

結果出力における応力ひずみ測度を指定する。
TYPE=SOLUTIONは!SOLUTIONで指定した解析種別で決まり、線形解析は線形の応力ひずみ、非線形解析は真応力対数ひずみが用いられる。
TYPE=MATERIALは材料種別で決まり、線形解析は線形の応力ひずみ、Updated Lagrange法で解かれる材料は真応力全ひずみ、
Total Lagrange法で解かれる材料は第2PK応力Greenひずみで出力される。

###### パラメータ

```
TYPE = SOLUTION(Default値)/MATERIAL
```
