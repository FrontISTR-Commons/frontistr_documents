##### `!SRADIATE` (4-9)

面グループによる輻射係数の定義

###### パラメータ

```
AMP1 = 輻射係数履歴テーブル名 (!AMPLITUDE で指定)
AMP2 = 雰囲気温度履歴テーブル名 (!AMPLITUDE で指定)
```

** 2行目以降 **

```
(2行目) SURFACE_GRP_NAME, Value, Sink
```

| 変数名           | 属性 | 内容         |
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | 面グループ名 |
| Value            | R    | 輻射係数     |
| Sink             | R    | 雰囲気温度   |

###### 使用例

```
!SRADIATE
  RSURF, 1.0E-9, 800.0
!SRADIATE, AMP2=TSRAD
  RSURF, 1.0E-9, 1.0
```
