###### `!deform_scale` (P1-14)

Specifies the displacement scale when displaying deformation.

Default: Auto

`standard_scale` = 0.1 * sqrt(`x_range`<sup>2</sup> + `y_range`<sup>2</sup> + `z_range`<sup>2</sup>) / `max_deform`

![Example of Display Styles Setting](media/analysis05_09.png){.center width="80%"}

**Figure 7.4.7: Example of display_styles Setting**

![Example of deform_scale Setting](media/analysis05_10.png){.center width="50%"}

**Figure 7.4.8: Example of deform_scale Setting**

