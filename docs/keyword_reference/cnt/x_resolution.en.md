##### `!x_resolution`, `!y_resolution` (P2-1, P2-2)

Specifies the resolution when `output_type=BMP`

![Example of x_resolution and y_resolution Setting](media/analysis05_12.png){.center width="80%"}

**Figure 7.4.10: Example of x_resolution and y_resolution Setting**

