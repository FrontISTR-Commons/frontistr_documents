##### `!ELASTIC` (2-2-1)

弾性材料の定義

###### パラメータ

```
TYPE = ISOTROPIC (Default値) / ORTHOTROPIC / USER
DEPENDENCIES = 0 (Default値) / 1
INFINITESIMAL    指定した場合、微小変形の前提が適用される
```

** 2行目以降 **

####### `TYPE = ISOTROPIC`の場合

(2行目) YOUNGS, POISSION, Temperature

| 変数名      | 属性 | 内容                             |
|-------------|------|----------------------------------|
| YOUNGS 　   | R    | ヤング率                         |
| POISSON     | R    | ポアソン比                       |
| Temperature | R    | 温度(`DEPENDENCIES`=1の時に必要) |

####### `TYPE= ORTHOTROPIC`の場合

```
(2行目) E1, E2, E3, ν12, ν13, ν23, G12, G13, G23, Tempreature
```
\[
\begin{bmatrix}
  \varepsilon_{11} \\\
  \varepsilon_{22} \\\
  \varepsilon_{33} \\\
  2\varepsilon_{12} \\\
  2\varepsilon_{23} \\\
  2\varepsilon_{31}
\end{bmatrix}
=
\begin{bmatrix}
  1/E_1 & -\nu_{12}/E_1 & -\nu_{13}/E_1 & 0        & 0        & 0 \\\
        & 1/E_2         & -\nu_{23}/E_2 & 0        & 0        & 0 \\\
	&               & 1/E_3         & 0        & 0        & 0 \\\
	&               &               & 1/G_{12} & 0        & 0 \\\
	& 対称          &               &          & 1/G_{23} & 0 \\\
	&               &               &          &          & 1/G_{31}
\end{bmatrix}
\begin{bmatrix}
  \sigma_{11} \\\
  \sigma_{22} \\\
  \sigma_{33} \\\
  \sigma_{12} \\\
  \sigma_{23} \\\
  \sigma_{31}
\end{bmatrix}
\]

####### `TYPE = USER`の場合

```
(2行目-10行目) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```
