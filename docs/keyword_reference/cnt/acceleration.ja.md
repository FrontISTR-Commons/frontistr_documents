#### `!ACCELERATION` (5-3)

加速度境界条件の定義

##### パラメータ

```
TYPE = INITIAL (初期加速度境界条件)
     = TRANSIT (AMPLITUDEで指定した時間歴加速度境界条件: デフォルト)
AMP  = 時間関数名 (!AMPLITUDEで指定)
       !AMPLITUDEで時間tと係数f(t)の関係を与える。
       下記 Value に係数f(t)を乗じた値がその時刻の拘束値になる
       (指定しない場合: 時間と係数関係はf(t) = 1.0となる)。
```

** 2行目以降 **

```
(2行目) NODE_ID, DOF_idS, DOF_idE, Value
```

| 変数名  | 属性 | 内容                         |
|---------|------|------------------------------|
| NODE_ID | I/C  | 節点番号または節点グループ名 |
| DOF_idS | I    | 拘束自由度の開始番号         |
| DOF_idE | I    | 拘束自由度の終了番号         |
| Value   | R    | 拘束値(デフォルト: 0)        |

##### 使用例

```
!ACCELERATION, TYPE=TRANSIT, AMP=AMP1
  1, 1, 3, 0.0
  ALL, 3, 3
  ※ 拘束値は0.0
!ACCELERATION, TYPE=INITIAL
  1, 3, 3, 1.0
  2, 3, 3, 1.0
  3, 3, 3, 1.0
```

  * 加速度境界条件の場合、変位境界条件の場合とは異なり、複数の自由度をまとめて定義できないため、DOF_idSとDOF_idEは同一番号でなければならない。
  * `TYPE`が`INITIAL`の場合、`AMP`が無効になる。
