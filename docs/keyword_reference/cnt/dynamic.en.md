##### `!DYNAMIC` (5-1)

Dynamic analysis control

Time t for each `!AMPLITUDE` specified in `!BOUNDARY`, `!CLOAD` and `!DLOAD` must be started from 0.0.

###### Parameter

```
TYPE = LINEAR    : Linear dynamic analysis
NONLINEAR : Nonlinear dynamic analysis
```

** 2nd line or later **

```
(2nd line) idx_eqa, idx_resp
```

|Parameter Name| Attributions| Contents|
|----------|------|-----------------------------------------------------------------------------------------------------------|
| idx_eqa  | I    | Solution of equation of motion (Direct time integration)<br/>(Default: 1)<br/>1: Implicit method (Newmark-&beta; method)<br/>11: Explicit method (Center difference method)|
| idx_resp | I    | Analysis type (Default: 1)<br/>1: Time history response analysis<br/>2: Frequency response analysis (Not included)|

####### `idx_resp=1` (Time history response analysis)

```
(3rd line) t_start , t_end , n_step, t_delta
```
|Parameter Name |Attributions |Contents|
|---------|------|---------------------------------------|
| t_start | R    |Analysis start time (Default: 0.0), not used|
| t_end   | R    |Analysis end time (Default: 1.0), not used|
| n_step  | I    |Overall No. of steps (Default: 1)|
| t_delta | R    |Time increment (Default: 1.0)|

```
(4th line) ganma , beta
```

|Parameter Name |Attributions |Contents
|--------|------|--------------------------------------------|
| ganma  | R    | Parameter &gamma; of Newmark-&beta; method (Default: 0.5)|
| beta   | R    | Parameter &beta; of Newmark-&gamma; method (Default: 0.25)|

```
(5th line) idx_mas ,idx_dmp , ray_m ,ray_k
```

|Parameter Name|Attributions|Contents|
|---------|------|-----------------------------------------------------------------------------------------------------|
| idx_mas | I    |Type of mass matrix (Default: 1)<br/>1: Lumped mass matrix<br/>2: Consistent mass matrix|
| idx_dmp | I    |1: Rayleigh damping (Default: 1)|
| ray_m   | R    |Parameter R<sub>m</sub> of Rayleigh damping (Default: 0.0)|
| ray_k   | R    |Parameter R<sub>k</sub> of Rayleigh damping (Default: 0.0)|

```
(6th line) nout, node_monit_1, nout_monit
```

|Parameter Name |Attributions |Contents|
|--------------|------|-------------------------------------|
| nout         | I    | not used|
| node_monit_1 | I    |Monitoring node ID (Global) or node group name|
| nout_monit   | I    |Results output interval of displacement monitoring<br/>(Default: 1)|

Note: Regarding the information of the monitoring node specified in this line, the displacement is output to the file &lt;dyna_disp_NID.txt&gt;, where NID is the global ID of the monitoring node, and each line includes the step number, time of the step, NID, u1, u2, and u3 in this order. The velocity and acceleration are also output to &lt;dyna_velo_NID.txt&gt; and &lt;dyna_acce_NID.txt&gt;, respectively, in the same format as the displacement. The nodal strain is output to &lt;dyna_strain_NID.txt&gt; and each line includes the step number, time of the step, NID, e11, e22, e33, e12, e23, and e13 in this order.  The nodal stress is output to &lt;dyna_stress_NID.txt&gt; and each line includes the step number, time of the step, NID, s11, s22, s33, s12, s23, s13, and s_mises in this order. When monitoring nodes are specified by a node group, each of the files stated above is separately output for each node. When this output is specified, the kinetic energy, deformation energy and the overall energy of the overall analytic model will also be output to &lt;dyna_energy.txt&gt;.

```
(7th line) iout_list(1),iout_list(2),iout_list(3),iout_list(4),iout_list(5),iout_list(6)
```

|Parameter Name|Attributions|Contents|
|--------------|------|----------------------------------------------------------------------|
| iout_list(1) | I    |Displacement output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(2) | I    |Velocity output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(3) | I    |Acceleration output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(4) | I    |Reaction force output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(5) | I    |Strain output specification (Default: 0)<br/>1: Output<br/>2: Output (Node base)<br/>3: Output (Element base)|
| iout_list(6) | I    |Stress output specification (Default: 0)<br/>0: Not output (Element base and node base)<br/>1: Output<br/>2: Output (Node base)<br/>3: Output (Element base)<br/>|

###### Example of Use

```
!DYNAMIC, TYPE=NONLINEAR
1 , 1
0.0, 1.0, 500, 1.0000e-5
0.5, 0.25
1, 1, 0.0, 0.0
100, 55, 1
0, 0, 0, 0, 0, 0
```

####### `idx_resp=2` (Frequency response analysis)

```
(3rd line) f_start, f_end, n_freq, f_disp
```

|Parameter Name |Attributions |Contents|
|---------|------|----------------------|
| f_start | R    |Minimum frequency|
| f_end   | R    |Maximum frequency|
| n_freq  | I    |Number of divisions for the frequency range|
| f_disp  | R    |Frequency to obtain displacement|

```
(4th line) t_start, t_end
```

|Parameter Name |Attributions |Contents|
|---------|------|------------------------|
| t_start | R    | Analysis start time|
| t_end   | R    | Analysis end time|

```
(5th line) idx_mas, idx_dmp, ray_m ,ray_k
```

|Parameter Name|Attributions|Contents|
|---------|------|-------------------------------------------------|
| idx_mas | I    | Type of mass matrix (Default: 1)<br/>1: Lumped mass matrix|
| idx_dmp | I    | 1: Rayleigh damping (Default: 1)|
| ray_m   | R    | Parameter R<sub>m</sub> of Rayleigh damping (Default: 0.0)
| ray_k   | R    | Parameter R<sub>k</sub> of Rayleigh damping (Default: 0.0)|

```
(6th line) nout, vistype, nodeout
```

|Parameter Name|Attributions|Contents|
|---------|------|-------------------------------------------------|
| nout    | I    |Results output interval in time domain|
| vistype | I    |Visuzalization type<br/>1:Mode shapes<br/>2:Time history results at f_disp|
| nodeout | I    |Monitoring NODE ID in frequency domain|

```
(7th line) iout_list(1),iout_list(2),iout_list(3),iout_list(4),iout_list(5),iout_list(6)
```

|Parameter Name|Attributions|Contents|
|--------------|------|----------------------------------------------|
| iout_list(1) | I    |Displacement output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(2) | I    |Velocity output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(3) | I    |Acceleration output specification (Default: 0)<br/>0: Not output, 1: Output|
| iout_list(4) | I    |not used|
| iout_list(5) | I    |not used|
| iout_list(6) | I    |not used|

###### Example of Use

```
!DYNAMIC
11 , 2
14000, 16000, 20, 15000.0
0.0, 6.6e-5
1, 1, 0.0, 7.2E-7
10, 2, 1
1, 1, 1, 1, 1, 1
```

