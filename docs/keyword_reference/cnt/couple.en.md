#### `!COUPLE` (5-4)

Definition of coupled surface (Used only in coupled analysis)

###### Parameter

```
TYPE =  1: One-way coupled (FrontISTR starts from receiving data)
2: One-way coupled (FrontISTR starts from sending data)
3: Staggered two-way coupled (FrontISTR starts from receiving data)
4: Staggered Two-way coupled (FrontISTR starts from sending data)
5: Iterative partitioned two-way coupled (FrontISTR starts from receiving data)
6: Iterative partitioned two-way coupled (FrontISTR starts from sending data)
ISTEP = Step No.
From the beginning of analysis to the step specified here, a linearly increasing
function from 0 to 1 is multiplied to the input fluid traction.
After this step, the input fluid traction is directly applied.
WINDOW => 0: Multiply window function(*) to input fluid traction
```

(\*) \(\frac{1}{2}(1 - \cos\frac{2\pi i}{N})\), \(i\): current step, \(N\): no. of steps of current analysis

** 2nd line or later **

```
(2nd line) COUPLING_SURFACE_ID
```

|Parameter Name|Attributions|Contents|
|------------|------|--------------|
| SURFACE_ID | C    |Surface group name|

###### Example of Use

```
!COUPLE , TYPE=1
SCOUPLE1
SCOUPLE2
```

