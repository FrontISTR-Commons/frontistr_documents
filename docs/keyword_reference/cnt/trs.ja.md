##### `!TRS` (2-2-8)

熱レオロジー単純化(Thermorheological Simplicity)による粘弾性材料の温度依存性の定義

この定義は`!VISCOELASTIC`の後ろに置かなければならない。前にある場合は、この定義が無視される。

###### パラメータ

```
DEFINITION = WLF(Default値)／ARRHENIUS
```

** 2行目以降 **

(2行目) θ<sub>0</sub>, C<sub>1</sub>, C<sub>2</sub>

| 変数名                       | 属性 | 内容     |
|------------------------------|------|----------|
| θ<sub>0</sub>                | R    | 参照温度 |
| C<sub>1</sub>, C<sub>2</sub> | R    | 材料定数 |
