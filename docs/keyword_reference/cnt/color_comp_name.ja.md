#### `!color_comp_name`, `!color_comp`, `!color_subcomp` (P1-5, P1-7, P1-8)

物理量からマラマップへの対応を指定する。必要な物理量やその自由度番号に名前をつける。これにより結果データの構造体node_label(:)やnn_dof(:)に名前がはいる。

Then you can define which one you hope to map into color by

##### `!color_comp_name` (文字列、省略値: 初めの変数)

```
例:

!color_comp_name = pressure
     静解析では  = DISPLASEMENT   : 結果変位データの指定
               = STRAIN         : ひずみデータの指定
               = STRESS         : 応力データの指定
    伝熱解析では = TEMPERATURE    : 結果温度データの指定
```

###### `!color_comp` (整数、省略値: 0)

物理量の識別番号 (0以上の整数)

```
例:

!color_comp = 2
```

結果データ種別の識別番号指定と成分名だが、未実装。

###### `!color_subcomp` (整数、省略値: 1)

物理量がベクトル量のような自由度数１以上の時、その自由度番号

```
例:
!color_subcomp = 0

    !color_comp_name=DISPLACEMENT指定の場合
        1: X成分,  2: Y成分, 3: Z成分

    !color_comp_name=STRAIN指定の場合
        1: εx,  2: εy,  3: εz
        4: εxy, 5: εyz, 6: εzx

    !color_comp_name=STRESS指定の場合
        1: σx, 2: σy, 3: σz
        4 : τxy 5 : τyz 6 : τzx

    !color/comp_name=TEMPERATURE指定の場合
        1: 温度
```

構造解析において例えば

| 物理量   | 変位 | ひずみ | 応力 |
|----------|------|--------|------|
| 自由度数 | 3    | 6      | 7    |

![color_comp, color_subcompおよびcolor_comp_nameの設定例](media/analysis05_07.png){.center width="80%"}

図7.4.5 `color_comp`, `color_subcomp`および`color_comp_name`の設定例
