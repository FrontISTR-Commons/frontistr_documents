##### `!CLOAD` (2-4)

Definition of concentrated load

###### Parameter

```
GRPID      = Group ID
AMP        = Time function name (Specified in !AMPLITUDE, valid in dynamic analysis)
ROT_CENTER = Node number of rotational constraint or node group name.
When specified it, this `!CLOAD` is recognized as load of torque.
```

** 2nd line or later **

```
(2nd line) NODE_ID, DOF_id, Value
```

|Parameters|Attributions|Contents                |
|---------|------|------------------------------|
| NODE_ID | I/C  |Node ID or node group name|
| DOF_id  | I    |Degree of freedom No.|
| Value   | R    |Load value|

###### Example of Use

```
!CLOAD, GRPID=1
1, 1, 1.0e3
ALL, 3, 10.0
!CLOAD, ROT_CENTER=7, GRPID=1
TORQUE_NODES, 1, 3
TORQUE_NODES, 3, -4
```

