##### `!EIGEN` (3-1)

Parameter settings of eigenvalue analysis

###### Parameter

N/A

** 2nd line or later **

```
(2nd line) NGET, LCZTOL, LCZMAX
```

|Parameter Name|Attributions |Contents         |
|---------|------|-----------------------------|
| NSET    | I    |No. of eigenvalue|
| LCZTOL  | R    |Allowance (Default: 1.0e-8)|
| LCZMAX  | I    |Max No. of iterations (Default: 60)|

###### Example of Use

```
!EIGEN
3, 1.0e-10, 40
```

#### Control Data for Heat Conduction Analysis

