##### `!HYPERELASTIC` (2-2-3)

超弾性材料の定義

###### パラメータ

```
TYPE = NEOHOOKE（Default値）
       MOONEY-RIVLIN
       ARRUDA-BOYCE
       MOONEY-RIVLIN-ANISO
       USER
```

** 2行目以降 **

####### `TYPE = NEOHOOKE`の場合

(2行目) C<sub>10</sub>, D

| 変数名       | 属性      | 内容          |
|--------------|-----------|---------------|
|C<sub>10</sub>| R         | 材料定数      |
|D 　          | R         | 材料定数      |

####### `TYPE = MOONEY-RIVLIN`の場合

(2行目) C<sub>10</sub>, C<sub>01</sub>, D

| 変数名      | 属性       | 内容          |
|-------------|------------|---------------|
|C<sub>10</sub>| R         | 材料定数      |
|C<sub>01</sub>| R         | 材料定数      |
|D             | R         | 材料定数      |

####### `TYPE = ARRUDA-BOYCE`の場合

```
(2行目) mu, lambda_m, D
```

| 変数名   | 属性 | 内容     |
|----------|------|----------|
| mu       | R    | 材料定数 |
| lambda_m | R    | 材料定数 |
| D        | R    | 材料定数 |

####### `TYPE = MOONEY-RIVLIN-ANISO`の場合

(2行目) C<sub>10</sub>, C<sub>01</sub>, D, C<sub>42</sub>, C<sub>43</sub>

| 変数名      | 属性       | 内容          |
|-------------|------------|---------------|
|C<sub>10</sub>| R         | 材料定数      |
|C<sub>01</sub>| R         | 材料定数      |
|D             | R         | 材料定数      |
|C<sub>42</sub>| R         | 材料定数      |
|C<sub>43</sub>| R         | 材料定数      |

####### `TYPE = USER`の場合

```
(2行目-10行目) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```
