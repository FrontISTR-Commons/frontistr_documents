##### `!WRITE, RESULT` (1-4)

解析結果データファイル出力を指定する。

###### パラメータ

```
FREQUENCY = 出力するステップ間隔 (デフォルト: 1)
```

###### 使用例

```
!WRITE, RESULT, FREQUENCY=2
```
