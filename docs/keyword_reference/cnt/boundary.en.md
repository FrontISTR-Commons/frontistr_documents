##### !BOUNDARY　(2-3)

Definition of displacement boundary conditions

###### Parameter

```
GRPID      = Group ID
AMP        = Time function name (Specified in !AMPLITUDE, valid in dynamic analysis)
ROT_CENTER = Node number of rotational constraint or node group name.
When specified it, this `!BOUNDARY` is recognized as rotational constraint.
TOTAL        When specified, the displacements are treated as total displacements from
the initial configuration (Default is relative displacements from the
configuration at the beginning of the step)
```

** 2nd line or later **

```
(2nd line) NODE_ID, DOF_idS, DOF_idE, Value
```

|Parameter Name |Attributions | Contents        |
|---------|------|------------------------------|
| NODE_ID | I/C  | Node ID or node group name|
| DOF_idS | I    | Start No. of restricted degree of freedom|
| DOF_idE | I    | End No. of restricted degree of freedom|
| Value   | R    | Restricted value (Default: 0)|

###### Example of Use

```
!BOUNDARY, GRPID=1
1, 1, 3, 0.0
ALL, 3, 3,
```
> Note: Resricted value is 0.0

