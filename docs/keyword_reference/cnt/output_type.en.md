###### `!output_type` (P1-19)

Specifies the type of output file. (Default: AVS)

```
AVS                   : UCD data for AVS (only on object surface)
BMP                   : Image data (BMP format)
VTK                   : VTK data for ParaView
COMPLETE_AVS          : UCD data for AVS
COMPLETE_REORDER_AVS  : Rearranges the node and element ID in the UCD data for AVS
SEPARATE_COMPLETE_AVS : UCD data for AVS for each decomposed domain
COMPLETE_MICROAVS     : Outputs the physical values in the scalar in the UCD data for AVS
BIN_COMPLETE_AVS      : Outputs COMPLETE_AVS in binary format
FSTR_FEMAP_NEUTRAL    : Neutral file for FEMAP
```

![Example of output_type](media/analysis05_11.png){.center width="80%"}

Figure 7.4.9: Example of output_type

