##### `!DLOAD` (2-5)

Definition of distributed load

###### Parameter

```
GRPID = Group ID
AMP = Time Function Name (Specified in !AMPLITUDE, valid in dynamic analysis)
FOLLOW = YES(Default) / NO
(whether pressure load follow deformation, valid in finite displacement analysis)
```

** 2nd Line or later **

```
(2nd line) ID_NAME, LOAD_type, param1, param2,...
```

|Parameter Name|Attributions|Contents|
|-----------|------|--------------------------------------------|
| ID_NAME   | I/C  | Surface group name, element group name, or element ID|
| LOAD_type | C    | Load type No.|
| param\*   | R    | Load parameter (refer to following)|


####### Load Parameters

|Load Type No.|Types                        |No. of Parameters| Parameter Array & Meaning                              |
|----------------|------------------------------|--------------|----------------------------------------------------------|
| S              |Applies pressure to surface specified in the surface group| 1            |  Pressure value                                                 |
| P0             |Applies pressure to shell element| 1            |Pressure value |
| PX             |Pressure to shell element along X direction| 1            |Pressure value |
| PY             |Pressure to shell element along Y direction| 1            |Pressure value |
| PZ             |Pressure to shell element along Z direction| 1            |Pressure value |
| P1             |Applies pressure to 1st surface| 1            |Pressure value |
| P2             |Applies pressure to 2nd surface| 1            |Pressure value |
| P3             |Applies pressure to 3rd surface| 1            |Pressure value |
| P4             |Applies pressure to 4th surface| 1            |Pressure value |
| P5             |Applies pressure to 5th surface| 1            |Pressure value |
| P6             |Applies pressure to 6th surface| 1            |Pressure value |
| BX             |Body force in X direction| 1            |Body force value|
| BY             |Body force in Y direction| 1            |Body force value|
| BZ             |Body force in Z direction| 1            |Body force value|
| GRAV           |Gravity| 4            |Gravitaional acceleration, gravity direction cosine|
| CENT           |Centrifugal force| 7            |Angular velocity, position vector at a point on the rotation axis, vector in the rotating axis direction|

####### Example of Use

```
!DLOAD, GRPID=1
1, P1, 1.0
ALL, BX, 1.0
ALL, GRAV, 9.8, 0.0, 0.0, -1.0
ALL, CENT, 188.495, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0
```

