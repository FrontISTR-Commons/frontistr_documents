###### `!output_type` (P1-19)

出力ファイルの型を指定する。 (省略値: AVS)

```
AVS                   : AVS用UCDデータ（物体表面上のみ）
BMP                   : イメージデータ（BMPフォーマット）
VTK                   : ParaView用VTKデータ
COMPLETE_AVS          : AVS用UCDデータ
COMPLETE_REORDER_AVS  : AVS用UCDデータで 節点・要素番号を並び替える
SEPARATE_COMPLETE_AVS : 分割領域ごとのAVS用UCDデータ
COMPLETE_MICROAVS     : AVS用UCDデータで物理量をスカラーで出力する
BIN_COMPLETE_AVS      : COMPLETE_AVSをバイナリー形式で出力する
FSTR_FEMAP_NEUTRAL    : FEMAP用ニュートラルファイル
```

![output_typeの例](media/analysis05_11.png){.center width="80%"}

図7.4.9　output_typeの例
