##### `!method` (P4-1)

面との切り口を指定する際、その面の設定方法を指定する。

```
!surface_num =2
!surface
!surface_style=3
!method=5
!coef=0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, -0.35
!color_comp_name = temperature
```

図7.4.17 `method`の設定例

これにより平面z=0.35 とz=-0.35. の切り口が可視化される。
