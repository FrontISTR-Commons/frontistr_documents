##### `!OUTPUT_RES` (1-7)

解析結果データファイル出力の物理量を指定する。

`!WRITE, RESULT`の指定が必要

###### パラメータ

なし

** 2行目以降 **

(2行目以降) 変数名、 ON／OFF

以下の変数名が指定可能である。

| 変数名         | 物理量                           |
|----------------|----------------------------------|
| DISP           | 変位(デフォルト出力)             |
| ROT            | 回転(761, 781シェル要素のみ有効) |
| REACTION       | 節点反力                         |
| NSTRAIN        | 節点ひずみ                       |
| NSTRESS        | 節点応力(デフォルト出力)         |
| NMISES         | 節点Mises応力(デフォルト出力)    |
| ESTRAIN        | 要素ひずみ                       |
| ESTRESS        | 要素応力(デフォルト出力)         |
| EMISES         | 要素Mises応力(デフォルト出力)    |
| ISTRAIN        | 積分点ひずみ                     |
| ISTRESS        | 積分点応力                       |
| PL_ISTRAIN     | 積分点相当塑性ひずみ             |
| TH_NSTRAIN     | 節点熱ひずみ(未実装)             |
| TH_ESTRAIN     | 要素熱ひずみ(未実装)             |
| TH_ISTRAIN     | 積分点熱ひずみ(未実装)           |
| VEL            | 速度                             |
| ACC            | 加速度                           |
| TEMP           | 温度                             |
| PRINC_NSTRESS  | 節点主応力(スカラ値)             |
| PRINCV_NSTRESS | 節点主応力(ベクトル値)           |
| PRINC_NSTRAIN  | 節点主ひずみ(スカラ値)           |
| PRINCV_NSTRAIN | 節点主ひずみ(ベクトル値)         |
| PRINC_ESTRESS  | 要素主応力(スカラ値)             |
| PRINCV_ESTRESS | 要素主応力(ベクトル値)           |
| PRINC_ESTRAIN  | 要素主ひずみ(スカラ値)             |
| PRINCV_ESTRAIN | 要素主ひずみ(ベクトル値)             |
| SHELL_LAYER    | 積層シェル要素の積層ごとの出力   |
| SHELL_SURFACE  | シェル要素の表面情報の出力       |
|CONTACT_NFORCE | 接触法線力（ベクトル値）         |
|CONTACT_FRICTION | 接触摩擦力（ベクトル値）       |
|CONTACT_RELVEL | 接触相対滑り速度（ベクトル値）＊スレーブ節点のみ|
|CONTACT_STATE  | 接触状態（スカラ値）＊スレーブ節点のみ。-1:解離, 0:未定義, 1:接触（固着）, 2:接触（滑り）|
|CONTACT_NTRACTION | 単位面積あたりの接触法線力（ベクトル値）|
|CONTACT_FTRACTION | 単位面積あたりの接触摩擦力（ベクトル値）|

###### 使用例

```
!OUTPUT_RES
  ESTRESS, OFF
  ISTRESS, ON
```
