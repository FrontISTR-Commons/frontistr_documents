##### `!CONTACT_ALGO` (2-7)

Specification of the contact analysis algorithm

###### Parameter

```
TYPE = SLAGRANGE (Lagrange multiplier method)
ALAGRANGE (Extended Lagrange multiplier method)
```

