##### `!USER_MATERIAL`　(2-2-10)

Input of user defined material

###### Parameter

```
NSTATUS = Specifies the number of state variables of material (Default: 1)
```

** 2nd line or later **

```
(2nd line-10th line) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```

