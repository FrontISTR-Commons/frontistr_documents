##### `!CLOAD` (2-4)

集中荷重の定義

###### パラメータ

```
GRPID      = グループID
AMP        = 時間関数名 (!AMPLITUDEで指定、動解析で有効)
ROT_CENTER = 回転中心節点番号または節点集合名。指定した場合、その !CLOAD はトルク荷重であると認識される。
```

** 2行目以降 **

```
(2行目) NODE_ID, DOF_id, Value
```

| 変数名  | 属性 | 内容                         |
|---------|------|------------------------------|
| NODE_ID | I/C  | 節点番号または節点グループ名 |
| DOF_id  | I    | 自由度番号                   |
| Value   | R    | 荷重値                       |

###### 使用例

```
!CLOAD, GRPID=1
  1, 1, 1.0e3
  ALL, 3, 10.0
!CLOAD, ROT_CENTER=7, GRPID=1
  TORQUE_NODES, 1, 3
  TORQUE_NODES, 3, -4
```

`ROT_NODES`に対して、節点7を中心とし、大きさ\(||(3, 0, -4)|| = 5\)、回転軸\((3/5, 0, -4/5)\)のトルク荷重を加える。

`ROT_CENTER`によるトルク荷重は、内部的には指定したトルク相当の節点荷重を与えるものであり、シェル要素に対する4, 5, 6自由度への節点荷重とは異なる。

`ROT_CENTER`を指定した場合

  * `ROT_CENTER`に指定する節点集合は、1つだけの節点からなる集合とする。
  * 1つの`!CLOAD`データブロックの中では、`NODE_ID`は全て同一にする。
