##### `!REFTEMP` (2-10)

Definition of reference temperature in thermal stress analysis

###### Parameter

N/A

** 2nd line or later **

```
(2nd line) Value
```

|Parameter Name|Attributions|Contents|
|--------|------|-------------------------|
| Value  | R    | Reference temperature (Default: 0)|

