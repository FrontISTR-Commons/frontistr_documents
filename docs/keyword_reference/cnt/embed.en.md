##### `!EMBED` (2-15)

Definition of embedding

###### Parameters

```
GRPID = boundary condition group ID
```

** 2nd line or later **

```
(2nd line or later) PAIR_NAME
```

| Variable Name | Attribute | Contents |
|-----------|------|--------------------------------------|
| PAIR_NAME | C | embedded pair name (defined in `!EMBED PAIR`)


###### Example usage

```
!CONTACT_ALGO, TYPE=SLAGRANGE
!EMBED, GRPID=1
IP1
!STEP
CONTACT,1
```

###### Notes

- To enable embedding in a step, specify the GRPID with the keyword “CONTACT” in the data line of the !STEP.

#### Control Data for Eigenvalue Analysis

