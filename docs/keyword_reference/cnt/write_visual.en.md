##### `!WRITE, VISUAL` (1-3)

Specifies the output data by the visualizer via memory.

###### Parameter

```
FREQUENCY = step interval of output (Default: 1)
```

###### Example of Use

```
!WRITE, VISUAL, FREQUENCY=2
```

