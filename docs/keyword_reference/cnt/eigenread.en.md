#### `!EIGENREAD` (5-5)

Controlling the input file for frequency response analysis

###### Parameter

N/A

** 2nd line or later **

|Parameter Name|Attributions|Contents|
|-------------------|------|----------------------------|
| eigenlog_filename | C    |The name of eigenvalue analysis log|

```
(3rd line) start_mode, end_mode
```

|Parameter Name|Attributions|Contents|
|------------|------|------------------------------------|
| start_mode | I    |lowest mode to be used in frequency response analysis|
| end_mode   | I    |highest mode to be used in frequency response analysis|

###### Example of Use

```
!EIGENREAD
eigen_0.log
1, 5
```

