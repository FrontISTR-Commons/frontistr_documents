##### `!SECTION` (1-11)

セクションの局所座標系および六面体1次要素(要素タイプ361)の定式化を指定する。
六面体1次要素の定式化はF-bar要素(FBAR)、B-Bar要素(BBAR)、非適合要素(IC)、完全積分要素(FI)が利用可能である。

###### パラメータ

```
SECNUM = メッシュデータ中の!SECTION入力順番号
ORIENTATION　=　局所座標系名
FORM361 = FBAR (非線形解析におけるDefault値)/IC (線形解析におけるDefault値)/BBAR/FI
FORM341 = FI (通常の四面体一次要素Default値)/SELECTIVE_ESNS(平滑化要素)
```
