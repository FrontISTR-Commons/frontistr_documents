##### `!FLUID` (2-2-9)

Definition of flow condition

###### Parameter

```
TYPE = INCOMP_NEWTONIAN (Default)
```

** 2nd Line or later **

```
(2nd line) mu
```

|Parameter Name|Attributions|Contents |
|--------|------|------|
| mu     | R    |Viscosity |

