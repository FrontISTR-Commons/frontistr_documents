##### `!MATERIAL`　(2-2)

Definition of material physical properties

The definition of the material physical properties is used in a set with the `!MATERIAL` and the `!ELASTIC`, `!PLASTIC` and etc. entered next. The `!ELASTIC`, `!PLASTIC` and etc. entered before `!MATERIAL` will be disregarded.

Note: When the `!MATERIAL` is defined in the analysis control data, the `!MATERIAL` definition in the mesh data will be disregarded. When the `!MATERIAL` is not defined in the analysis control data, the `!MATERIAL` definition in the mesh data is used.

###### Parameter

```
NAME = Material name
```

