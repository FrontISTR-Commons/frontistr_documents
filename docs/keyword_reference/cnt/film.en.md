##### `!FILM` (4-6)

Definition of heat transfer coefficient given to the boundary plane

###### Parameter

```
AMP1 = Heat transfer coefficient history table name (specified in !AMPLITUDE)
AMP2 = Ambient temperature history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) ELEMENT_GRP_NAME, LOAD_type, Value, Sink
```

|Parameter Name| Attributions| Contents|
|------------------|------|------------------------------|
| ELEMENT_GRP_NAME | C/I  | Element group name or element ID |
| LOAD_type        | C    | Load type No. |
| Value            | R    | Heat transfer coefficient |
| Sink             | R    | Ambient temperature |

###### Example of Use

```
!FILM
FSURF, F1, 1.0, 800.0
!FILM, AMP1=TFILM
FSURF, F1, 1.0, 1.0
```

####### Load Parameters

|Load Type No.|Applied Surface | Parameter|
|----------------|----------|------------------------|
| F1             | Surface No. 1| Heat transfer coefficient and ambient temperature|
| F2             | Surface No. 2| Heat transfer coefficient and ambient temperature|
| F3             | Surface No. 3| Heat transfer coefficient and ambient temperature|
| F4             | Surface No. 4| Heat transfer coefficient and ambient temperature|
| F5             | Surface No. 5| Heat transfer coefficient and ambient temperature|
| F6             | Surface No. 6| Heat transfer coefficient and ambient temperature|
| F0             | Shell Surface| Heat transfer coefficient and ambient temperature|

