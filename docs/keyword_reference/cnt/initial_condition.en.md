##### `!INITIAL_CONDITION` (1-14)

Definition of initial condition

###### Parameter

```
TYPE = TEMPERATURE/VELOCITY/ACCELERATION
```

####### In case of `TYPE = TEMPERATURE`

(2nd line) ng1, t1

(3rd line or later) ng2, t2

...

|Parameter Name|Attributions|Contents                      |
|---------------|-----------|---------------|
| ng1,ng2, ...    | C/I         | name of node group/index of node |
| t1, t2, ...    | R         | temperature |

####### In case of `TYPE= VELOCITY/ACCELERATION`

(2nd line) ng1, dof1, v1

(3rd line or later) ng2, dof2, v2

...

|Parameter Name|Attributions|Contents                      |
|---------------|-----------|---------------|
| ng1,ng2, ...    | C/I         | name of node group/index of node |
| dof1, dof2, ... | I         | dof number(1-6) |
| v1, v2, ...    | R         | velocity/acceleration |

