#### `!SOLVER` (6-1)

Control of solver

Mandatory control data

##### Parameter

```
METHOD =    Method (CG, BiCGSTAB, GMRES, GPBiCG, DIRECT, DIRECTmkl, MUMPS)
DIRECT: Direct method other than contact analysis (serial processing only) (currently unavailable)
DIRECTmkl: Direct method by Intel MKL
MUMPS    : Direct method by MUMPS
When any of direct methods is selected, the data lines will be disregarded.
Thread-parallel computation by OpenMP is available in iterative methods
for 3D problems.

PRECOND =   Preconditioner for iterative methods (1, 2, 3, 5, 10, 11, 12)
1, 2       : (Block) SSOR (with multi-color ordering only for 3D problems)
3          : (Block) Diagonal Scaling
5          : AMG by multigrid preconditioner package ML
10         : Block ILU(0)
11         : Block ILU(1)
12         : Block ILU(2)
10, 11 and 12 are available only in 3D problems.
In thread-parallel computation, SSOR, Diagonal Scaling or ML is recommended.

ITERLOG =   Whether solver convergence history is output (YES/NO) (Default: NO)

TIMELOG =   Whether solver computation time is output (YES/NO/VERBOSE) (Default: NO)

USEJAD =    Whether matrix ordering optimized for vector processors are performed
(YES/NO) (Default: NO)
Valid only in 3D problems with iterative solvers.

SCALING =   Whether matrix is scaled so that each diagonal element becomes 1 (YES/NO)
(Default: NO)
Valid only in 3D problems with iterative solvers.

DUMPTYPE =  Type of matrix dumping (NONE, MM, CSR, BSR) (Mainly for debugging)
NONE : no dumping (Default)
MM   : matrix is dumped in Matrix Market format
CSR  : matrix is dumped in Compressed Sparse Row (CSR) format
BSR  : matrix is dumped in Blocked CSR format

DUMPEXIT =  Whether the program terminates right after matrix dumping (YES/NO)
(Default: NO)

MPCMETHOD = Method for multipoint constraints
1: Penalty method (Default for direct methods)
2: MPC-CG method (Deprecated)
3: Explicit master-slave elimination (Default for iterative methods)

ESTCOND =   Frequency of condition number estimation (experimental)
Estimation is performed at every specified number of iterations and at the last
iteration.  No estimation when 0 is specified.

METHOD2 =   Secondary method (BiCGSTAB, GMRES, GPBiCG) (experimental)
Valid only when CG is specified as METHOD.
When specified, the method is swithced and solution continues when CG diverged.
All the other parameters and data lines are shared with the CG method.

CONTACT_ELIM = Whether DOF elimination is performed in contact analysis (0, 1)
0: Perform DOF elimination only when using iterative methods (Default)
1: Always perform DOF elimination (even when using direct methods)
```

** 2nd line or later **

```
(2nd line) NITER, iterPREmax, NREST, NCOLOR_IN, RECYCLEPRE
```

|Parameter Name|Attributions|Contents|
|------------|------|---------------------------|
| NITER      | I    |No. of iterations (Default: 100)
| iterPREmax | I    |No. of iteration of preconditioning based on Additive Schwarz<br/>(Default: 1)<br/>(recommended value : 1 (2 might be efficient in some parallel computation))|
| NREST      | I    |No. of Krylov subspaces (Default: 10)<br/>(Valid only when GMRES is selected as the solution)|
| NCOLOR_IN  | I    |No. of Colors for Multi-Color ordering (Default: 10)<br/>(Valid only when no. of OpenMP threads >= 2)|
| RECYCLEPRE | I    |No. of recycling set-up info for preconditioning (Default: 3)<br/>(Valid only in nonlinear analyses)|

```
(3rd line) RESID, SIGMA_DIAG, SIGMA
```

|Parameter Name|Attributions|Contents|
|------------|------|--------------------------------------|
| RESID      | R    |Truncation error (Default: 1.0e-8)|
| SIGMA_DIAG | R    |Scale factor for diagonal elements when computing preconditioning matrix (Default: 1.0)<br/>(When divide-by-zero or divergence occurs with ILU preconditioning, convergence might be obtained by setting number greater than 1.0)|
| SIGMA      | R    |Not used (Default: 0.0)|


####### In case of `PRECOND=5` (Optional)

When any other value is specified for PRECOND, the 4th line will be disregarded.

```
(4th line) ML_CoarseSolver, ML_Smoother, ML_MGCycle, ML_MaxLevels, ML_CoarseningScheme, ML_NumSweep
```

|Parameter Name|Attributions|Contents|
|---------------------|------|--------------------------------------|
| ML_CoarseSolver     | I    | Coarse solver of ML (1: smoother, 2: KLU (serial direct solver), 3: MUMPS (parallel direct solver)) (Default: 1) <br/>(recommended value : 3 or 2 for stiff problems, 1 for other problems)  |
| ML_Smoother         | I    | Smoother of ML (1: Chebyshev, 2: SymBlockGaussSeidel, 3: Jacobi) (Default: 1) <br/>(recommended value : 1)  |
| ML_MGCycle          | I    | Multigrid cycle of ML (1: V-cycle, 2: W-cycle, 3: Full-V-cycle) (Default: 1) <br/>(recommended value : 2 for stiff problems, 1 for other problems)  |
| ML_MaxLevels        | I    | Max No. of levels of ML (Default: 10) <br/>(recommended value : 2 (or 3 when memory is not sufficient) with direct coarse solver for very stiff problems, 10 for other problems)  |
| ML_CoarseningScheme | I    | Coarsening scheme of ML (1: UncoupledMIS, 2: METIS, 4: Zoltan, 5: DD) (Default: 1) <br/>(recommended value : 1 or 5)  |
| ML_NumSweep         | I    | No. of smoother sweeps of ML (polinomial degree for Chebyshev) (Default: 2) <br/>(recommended value : 2 for Chebyshev, 1 for SymBlockGaussSeidel)   |



###### Example of Use

Use CG with SSOR preconditioning, and set No. of iteration to 10000 and truncation error to 1.0e-8

```
!SOLVER, METHOD=CG, PRECOND=1, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.0, 0.0
```

Use GMRES with SSOR preconditioning, and set No. of Krylov subspace to 40 and No. of colors for Multi-Color ordering to 100

```
!SOLVER, METHOD=GMRES, PRECOND=1, ITERLOG=YES, TIMELOG=YES
10000, 1, 40, 100
1.0e-8, 1.0, 0.0
```

Use CG with ILU(0) preconditioning, and set scale factor for diagonal elements when computing preconditioning matrix to 1.1

```
!SOLVER, METHOD=CG, PRECOND=10, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.1, 0.0
```

Use CG with AMG preconditioning by ML

```
!SOLVER, METHOD=CG, PRECOND=5, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.0, 0.0
```

Use CG with AMG preconditioning by ML, and set coarse solver to MUMPS (for stiff problems)

```
!SOLVER, METHOD=CG, PRECOND=5, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.0, 0.0
3
```

Use CG with AMG preconditioning by ML, and set multigrid cycle to W-cycle (for stiff problems)

```
!SOLVER, METHOD=CG, PRECOND=5, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.0, 0.0
1, 1, 2
```

Use CG with AMG preconditioning by ML, and set coarse solver to MUMPS and max No. of levels to 2 (for very stiff problems)

```
!SOLVER, METHOD=CG, PRECOND=5, ITERLOG=YES, TIMELOG=YES
10000, 1
1.0e-8, 1.0, 0.0
3, 1, 1, 2
```


#### Post Process (Visualization) Control Data

