##### `!display_method` (P1-4)

表示方法 (省略値: 1)

  1. 色コードの表示
  2. 境界線表示
  3. 色コード及び境界線表示
  4. 指定色一色の表示
  5. 色分けにによる等値線表示

![display_methodの設定例](media/analysis05_06.png){.center width="80%"}

図7.4.4　display_methodの設定例
