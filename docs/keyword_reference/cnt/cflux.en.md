##### `!CFLUX` (4-3)

Definition of centralized heat flux given to the node

###### Parameter

```
AMP = Flux history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) NODE_GRP_NAME, Value
```

|Parameter Name| Attributions |Contents|
|---------------|------|------------------------------|
| NODE_GRP_NAME | C/I  |Node group name or node ID|
| Value         | R    |Heat flux value|

###### Parameter

```
!CFLUX
ALL, 1.0E-3
!CFLUX, AMP=FUX1
ALL, 1.0
```

