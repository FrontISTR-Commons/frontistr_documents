##### `!VISCOELASTIC` (2-2-4)

Definition of viscoelastic material

`!VISCOELASTIC` must be defined together with `!ELASTIC`.

###### Parameter

```
DEPENDENCIES = the number of parameters depended upon (Not included)
INFINITESIMAL    When specified, infinitesimal deformation is assumed
```

** 2nd Line or later **

```
(2nd line) g, t
```

|Parameter Name |Attributions |Contents            |
|--------|------|------------------|
| g      | R    | Shear relaxation modulus |
| t      | R    | Relaxation time        |

