##### `!viewpoint`, `!look_at_point`, `!up_direction` (P2-5, P2-6, P2-7)

Specifies the viewpoint position by coordinates.

Default: x = (xmin + xmax)/2.0, y = ymin + 1.5 *(ymax – ymin), z = zmin + 1.5 *(zmax – zmin)

###### **look_at_point**

Specifies the look at point position.

Default: Center of data

###### **up_direction**

Specifies the view frame in viewpoint, look_at_point and up_direction.

default: 0.0 0.0 1.0

###### View coodinate frame

- Origin: `look_at_point`
- Z-axis: `viewpoint` - `look_at_point`
- X-axis: `up_direction` &times; z axis

![View Frame Determination Method](media/analysis05_13.png){.center width="50%"}

**Figure 7.4.11: View Frame Determination Method**

![Example of !viewpoint, look_at_point and up_direction Setting](media/analysis05_14.png){.center width="80%"}

**Figure 7.4.12: Example of !viewpoint, look_at_point and up_direction Setting**


