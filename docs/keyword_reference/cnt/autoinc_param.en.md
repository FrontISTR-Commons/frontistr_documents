##### `!AUTOINC_PARAM` (2-12)

Specify auto-incremental parameters.

###### Parameter

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
|NAME          | C         | Automatic incremental parameter name (required) |

** 2nd line **

Specify the reduction conditions and the rate of time incremental reduction.

```
(2nd line) RS, NS_MAX, NS_SUM, NS_COUT, N_S
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
|RS            | R         | Time incremantal rate of decline (default:0.25) |
|NS_MAX        | I         | Threshold for maximum number of Netwon method iterations (default: 10) |
|NS_SUM        | I         | Threshold for the total number of Netwon method iterations (default: 50) |
|NS_CONT       | I         | Number of contact iterations threshold (default: 10) |
|N_S           | I         | Number of sub-steps until the reduction condition is met (default: 1) |

** 3rd line **

Specifies the condition for the increase and the rate of increase of the time increment at that time.

```
(3rd line) RL, NL_MAX, NL_SUM, NL_COUT, N_L
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
|RL            | R         | Incremental rate of increase by time (default:1.25) |
|NL_MAX        | I         | Threshold for maximum number of Netwon method iterations (default: 1) |
|NL_SUM        | I         | Threshold for the total number of Netwon method iterations (default: 1) |
|NL_CONT       | I         | Number of contact iterations threshold (default: 1) |
|N_L           | I         | Number of sub-steps until the increase condition is met (default: 2) |

** 4th line **

```
(4th line) RC, N_C
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| RC           | R         | Decrease of time increment at cutback (default: 0.25)|
| N_C          | I         | Maximum permissible number of continuous cutbacks (default: 5) |

###### example

With the same settings as the default settings

```
!AUTOINC_PARAM, NAME=AP1
0.25, 10, 50, 10, 1
1.25,  1,  1,  1, 2
0.25,  5
```

