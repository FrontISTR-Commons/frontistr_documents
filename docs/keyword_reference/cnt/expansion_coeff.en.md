##### `!EXPANSION_COEFF`(2-2-7)

Definition of coefficient of linear expansion

The coefficient to be input here is not the coefficient of linear expansion \(\alpha\) at each temperature,
but its averaged value between the reference temperature \(T_{ref}\) and each temperature \(T\) as follows:

\begin{equation}
\overline{\alpha}(T)=\frac{1}{T-T_{ref}} \int_{T_{ref}}^T \alpha(T) dT
\end{equation}

###### Parameter

```
TYPE = ISOTROPIC(Default) / ORTHOTROPIC
DEPENDENCIES = 0(Default) / 1
```

** 2nd Line or later **

####### In case of `TYPE=ISOTROPIC`

```
(2nd line) expansion, Temperature
```

####### In case of `TYPE=ORTHOTROPIC`

```
(2nd line) $\alpha$11, $\alpha$22, $\alpha$33, Temperature
```

|Parameter Name|Attributions | Contents|
|---------------|------|--------------------------------|
| expansion     | R    | Coefficient of thermo expansion|
| $\alpha$11, $\alpha$22, $\alpha$33 | R    | Coefficient of thermo expansion|
| Tempearture   | R    | Temperature (required when DEPENDENCIES = 1)|

