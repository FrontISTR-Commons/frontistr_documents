##### `!SPRING` (2-3-1)

Definition of spring boundary conditions

###### Parameter

```
GRPID = Group ID
```

** 2nd line or later **

```
(2nd line) NODE_ID, DOF_id, Value
```

|Parameter Name|Attributions|Contents|
|---------|------|------------------------------|
| NODE_ID | I/C  | Node ID or node group name |
| DOF_id  | I    | Restricted degree of freedom |
| Value   | R    | Spring constant|

###### Example of Use

```
!SPRING, GRPID=1
1, 1, 0.5
```

