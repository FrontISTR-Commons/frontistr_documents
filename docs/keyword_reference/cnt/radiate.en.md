##### `!RADIATE` (4-8)

Definition of radiation factor given to boundary plane

###### Parameter

```
AMP1 = Radiation factor history table name (specified in !AMPLITUDE)
AMP2 = Ambient temperature history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) ELEMENT_GRP_NAME, LOAD_type, Value, Sink
```

|Parameter Name |Attributions |Contents|
|------------------|------|------------------------------|
| ELEMENT_GRP_NAME | C/I  | Element group name or element ID|
| LOAD_type        | C    | Load type No.|
| Value            | R    | Radiation factor|
| Sink             | R    | Ambient temperature|

###### Example of Use

```
!RADIATE
RSURF, R1, 1.0E-9, 800.0
!RADIATE, AMP2=TRAD
RSURF, R1, 1.0E-9, 1.0
```

####### Load Parameters

|Load Type No. | Applied Surface |Parameter|
|----------------|----------|----------------------|
| R1             |Surface No. 1| Radiation factor and ambient temperature|
| R2             |Surface No. 2| Radiation factor and ambient temperature|
| R3             |Surface No. 3| Radiation factor and ambient temperature|
| R4             |Surface No. 4| Radiation factor and ambient temperature|
| R5             |Surface No. 5| Radiation factor and ambient temperature|
| R6             |Surface No. 6| Radiation factor and ambient temperature|
| R0             |Shell Surface| Radiation factor and ambient temperature|

