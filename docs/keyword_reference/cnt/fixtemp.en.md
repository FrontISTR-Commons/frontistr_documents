##### `!FIXTEMP` (4-2)

Definition of fixed temperature

###### Parameter

```
AMP = Flux history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) NODE_GRP_NAME, Value
```

|Parameter Name|Attributions|Contents|
|---------------|------|------------------------------|
| NODE_GRP_NAME | C/I  | Node group name or node ID |
| Value         | R    | Temperature (Default: 0) |

###### Example of Use

```
!FIXTEMP
ALL, 20.0
!FIXTEMP, AMP=FTEMP
ALL, 1.0
```

