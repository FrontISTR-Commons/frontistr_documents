##### `!SFLUX` (4-5)

面グループによる分布熱流束の定義

###### パラメータ

```
AMP = 流束履歴テーブル名 (!AMPLITUDE で指定)
```

** 2行目以降 **

```
(2行目) SURFACE_GRP_NAME, Value
```

| 変数名           | 属性 | 内容         |
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | 面グループ名 |
| Value            | R    | 熱流束値     |

###### 使用例

```
!SFLUX
  SURF, 1.0
!SFLUX, AMP=FLUX3
  SURF, 1.0
```
