###### `!deform_scale` (P1-14)

変形を表示する際の変位スケールを指定する。

Default: 自動

`standard_scale` = 0.1 * sqrt(`x_range`<sup>2</sup> + `y_range`<sup>2</sup> + `z_range`<sup>2</sup>) / `max_deform`

![display_stylesの設定例](media/analysis05_09.png){.center width="80%"}

図7.4.7 `display_styles`の設定例

![deform_scaleの設定例](media/analysis05_10.png){.center width="50%"}

図7.4.8 `deform_scale`の設定例
