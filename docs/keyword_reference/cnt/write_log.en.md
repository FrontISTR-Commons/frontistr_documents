##### `!WRITE, LOG` (1-5)

Specifies the step interval for output to the log file.

###### Parameter

```
FREQUENCY = step interval of output (Default:1)
```

###### Example of Use

```
!WRITE, LOG, FREQUENCY=2
```

