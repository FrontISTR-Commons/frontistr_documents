##### `!color_mapping_bar_on`, `!scale_marking_on`, `!num_of_scales` (P2-16, P2-17, P2-18)

|      |      |
|------|------|
|`!color_mapping_bar_on`|Specifies whether to display the color mapping bar.<br/>0: off 1: on (Default: 0)|
|`!scale_marking_on`    |set the memory status of `color_mapping_bar`<br/>0: off 1: on (default: 0)|
|`!num_of_scales`       |Specifies the number of memory. <br/> (default: 3)|

![Example of Color Mapping Bar Display](media/analysis05_16.png){.center width="80%"}

