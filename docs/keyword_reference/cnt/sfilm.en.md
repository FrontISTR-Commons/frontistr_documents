##### `!SFILM` (4-7)

Definition of heat transfer coefficient by surface group

###### Parameter

```
AMP1 = Heat transfer coefficient history table name (specified in !AMPLITUDE)
AMP2 = Ambient temperature history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) SURFACE_GRP_NAME, Value, Sink
```

|Parameter Name| Attributions| Contents|
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | Surface group name|
| Valu             | R    | Heat Transfer Rate|
| Sink             | R    | Ambient Temperature|

###### Example of Use

```
!SFILM
SFSURF, 1.0, 800.0
!SFILM, AMP1=TSFILM, AMP2=TFILM
SFSURF, 1.0, 1.0
```

