##### `!OUTPUT_RES` (1-7)

Output item control of the result

`!WRITE, RESULT` must be specified

###### Parameter

N/A

###### 2nd Line or later

```
(2nd line or later) Parameter name, ON/OFF
```

The following parameter names can be specified.

|Parameter Names | Physical Values |
|------------|---------------------|
|`DISP`      |Displacement (Default output)|
|`ROT`       |Rotation (Only for 781,761 shell)|
|`REACTION`  |Reaction force of nodes|
|`NSTRAIN`   |Strain of nodes|
|`NSTRESS`   |Stress of nodes (Default output)|
|`NMISES`    |Mises stress of nodes (Default output)|
|`ESTRAIN`   |Strain of elements|
|`ESTRESS`   |Stress of elements (Default output)|
|`EMISES`    |Mises stress of elements (Default output)|
|`ISTRAIN`   |Strain of integration points|
|`ISTRESS`   |Stress of integration points|
|`PL_ISTRAIN`|Plastic strain of integration points|
|`TH_NSTRAIN`|Thermal strain of nodes (Not included)|
|`TH_ESTRAIN`|Thermal strain of elements (Not included)|
|`TH_ISTRAIN`|Thermal strain of integration points (Not included)|
|`VEL`       |Velocity|
|`ACC`       |Acceleration|
|`TEMP`          | Temperature|
|`PRINC_NSTRESS` | Nodal principal stress(Scalar value)|
|`PRINCV_NSTRESS`| Nodal principal stress(Vector value)|
|`PRINC_NSTRAIN` | Nodal principal strain(Scalar value)|
|`PRINCV_NSTRAIN`| Nodal principal strain(Vector value)|
|`PRINC_ESTRESS` | Elemental principal stress(Scalar value)|
|`PRINCV_ESTRESS`| Elemental principal stress(Vector value)|
|`PRINC_ESTRAIN` | Elemental principal strain(Scalar value)|
|`PRINCV_ESTRAIN`| Elemental principal strain(Vector value)|
|`SHELL_LAYER`   | Output per layer of layerd shell element|
|`SHELL_SURFACE` | Output of surface information of shell element|
|`CONTACT_NFORCE` | Contact normal force(Vector value) |
|`CONTACT_FRICTION` | Contact friction force(Vector value) |
|`CONTACT_RELVEL` | Contact relative displacement (Vector value / slave point only)|
|`CONTACT_STATE`  | Contact state(Scalar value / -1, 0, 1 and 2 means free, undefined, stick and slip respectively)|
|`CONTACT_NTRACTION` | Contact normal traction(Vector value)|
|`CONTACT_FTRACTION` | Contact friction traction(Vector value)|

###### Example of Use

```
! OUTPUT_RES
ESTRESS, OFF
ISTRESS, ON
```

