##### `!initial_style`, `!deform_style` (P1-15, P1-16)

Specifies the display style of the initial shape and the deformed shape.

0. Not specified
1. Solid line mesh (Displayed in blue if not specified)
2. Gray filled pattern
3. Shading (Let the physical attributions respond to the color)
4. Dotted line mesh (Displayed in blue if not specified)

