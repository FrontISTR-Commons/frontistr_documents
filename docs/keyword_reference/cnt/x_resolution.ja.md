##### `!x_resolution`, `!y_resolution` (P2-1, P2-2)

`output_type=BMP`の時、解像度を指定する。

![x_resolutionとy_resolutionの設定例](media/analysis05_12.png){.center width="80%"}

図7.4.10 `x_resolution`と`y_resolution`の設定例
