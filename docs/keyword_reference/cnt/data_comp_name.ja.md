##### `!data_comp_name`, `!data_comp`, `!data_subcomp` (P3-1, P3-3, P3-4)

`surface_style=2`の時、可視化する等値面の物理量を指定する。

![data_comp, data_subcomp及びdata_comp_nameの設定例](media/analysis05_18.png){.center width="80%"}

図7.4.16 `data_comp`, `data_subcomp`及び`data_comp_name`の設定例
