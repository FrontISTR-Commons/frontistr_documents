##### `!PLASTIC` (2-2-2)

Definition of plastic material

`!PLASTIC` must be defined together with `!ELASTIC`.

###### Parameter

```
YIELD        = MISES (Default), Mohr-Coulomb, DRUCKER-PRAGER, USER
HARDEN       = BILINEAR (Default), MULTILINEAR, SWIFT, RAMBERG-OSGOOD,
KINEMATIC, COMBINED
DEPENDENCIES = 0 (Default)/1
INFINITESIMAL    When specified, infinitesimal deformation is assumed
```

** 2nd line or later **

####### In case of `YIELD = MISES` (Default)

** In case of `HARDEN = BILINEAR` (Default) **

```
(2nd line) YIELD0, H
```

** In case of `HARDEN = MULTILINEAR` **

```
(2nd line) YIELD, PSTRAIN, Temperature
(3rd line) YIELD, PSTRAIN, Temperature

...continues
```

** In case of `HARDEN = SWIFT` **

```
(2nd line) $\epsilon$0, K, n
```

** In case of `HARDEN = RAMBERG-OSGOOD` **

```
(2nd line) $\epsilon$0, D, n
```

** In case of `HARDEN = KINEMATIC` **

```
(2nd line) YIELD0, C
```

** In case of `HARDEN = COMBINED` **

```
(2nd line) YIELD0, H, C
```

####### In case of `YIELD =` Mohr-Coulomb or Drucker-Prager

** In case of `HARDEN = BILINEAR`(Default) **

```
(2nd line) c, $\phi$, H, $\psi$
```

** In case of `HARDEN = MULTILINEAR` **

```
(2nd line) $\phi$, $\psi$
(3nd line) c, PSTRAIN
(4th line) c, PSTRAIN
...continues
```

`HARDEN = `others will be disregarded, becomes the default (`BILINEAR`).

| Parameter Name    | Attributions | Contents                        |
|------------|------|------------------------------------------------|
| YIELD0     | R    | Initial yield stress                           |
| H          | R    | Hardening factor                               |
| PSTRAIN    | R    | Plastic strain                                 |
| YIELD      | R    | Yield stress                                   |
| \(\varepsilon0, K, n\)   | R    |\(\overline{\sigma} = k\left( \varepsilon_{0} + \overline{\varepsilon} \right)^{n}\)|
| \(\varepsilon0, D, n\)   | R    |\(\varepsilon = \frac{\sigma}{E} + \varepsilon_{0}\left( \frac{\sigma}{D} \right)^{n}\)|
| $\phi$     | R    | Angle of internal friction                     |
| $\psi$     | R    | Dilatancy angle (Default: same value as $\phi$)|
| c          | R    | Cohesion                                       |
| C          | R    | Linear motion hardening factor                 |
| Tempearture| R    | Temperature (required when `DEPENDENCIES = 1`) |
| v1, v2...v10 | R  | Material constant                              |

** In the case of `YIELD= USER` **

```
(2nd Line or later) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```

###### Example of Use

```
!PLASTIC, YIELD=MISES, HARDEN=MULTILINEAR, DEPENDENCIES=1
276.0, 0.0, 20.
296.0, 0.0018, 20.
299.0, 0.0053, 20.
303.0, 0.008, 20.
338.0, 0.0173, 20.
372.0, 0.0271, 20.
400.0, 0.037, 20.
419.0, 0.0471, 20.
437.0, 0.0571, 20.
450.0, 0.0669, 20.
460.0, 0.0767, 20.
469.0, 0.0867, 20.
477.0, 0.0967, 20.
276.0, 0.0, 100.
276.0, 0.0018, 100.
282.0, 0.0053, 100.
295.0, 0.008, 100.
330.0, 0.0173, 100.
370.0, 0.0271, 100.
392.0, 0.037, 100.
410.0, 0.0471, 100.
425.0, 0.0571, 100.
445.0, 0.0669, 100.
450.0, 0.0767, 100.
460.0, 0.0867, 100.
471.0, 0.0967, 100.
128.0, 0.0, 400.
208.0, 0.0018, 400.
243.0, 0.0053, 400.
259.0, 0.008, 400.
309.0, 0.0173, 400.
340.0, 0.0271, 400.
366.0, 0.037, 400.
382.0, 0.0471, 400.
396.0, 0.0571, 400.
409.0, 0.0669, 400.
417.0, 0.0767, 400.
423.0, 0.0867, 400.
429.0, 0.0967, 400.
```

The work hardening coefficient will be calculated by inserting the data from the above inputdata, regarding the specified temperature or plastic strain. It is necessary to input the same `PSTRAIN` array for each temperature.

