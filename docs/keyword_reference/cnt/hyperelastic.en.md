##### `!HYPERELASTIC` (2-2-3)

Definition of hyperelastic material

###### Parameter

```
TYPE = NEOHOOKE (Default)
MOONEY-RIVLIN
ARRUDA-BOYCE
MOONEY-RIVLIN-ANISO
USER
```

###### 2nd Line or later

####### In the case of `TYPE = NEOHOOKE`

(2nd line) C<sub>10</sub>, D

|Parameter Name|Attributions|Contents         |
|--------------|-----------|---------------|
|C<sub>10</sub>| R         |Material constant     |
|D             | R         |Material constant     |

####### In case of `TYPE = MOONEY-RIVLIN`

(2nd line) C<sub>10</sub>, C<sub>01</sub>, D

|Parameter Name|Attributions|Contents |
|-------------|------------|---------------|
|C<sub>10</sub>| R         |Material constant|
|C<sub>01</sub>| R         |Material constant|
|D             | R         |Material constant|

####### In case of `TYPE = ARRUDA-BOYCE`

```
(2nd line) mu, lambda_m, D
```

|Parameter Name|Attributions|Contents    |
|----------|------|----------|
| mu       | R    | Material constant |
| lambda_m | R    | Material constant |
| D        | R    | Material constant |

####### `TYPE = MOONEY-RIVLIN-ANISO`の場合

(2nd line) C<sub>10</sub>, C<sub>01</sub>, D, C<sub>42</sub>, C<sub>43</sub>

| 変数名      | 属性       | 内容          |
|-------------|------------|---------------|
|C<sub>10</sub>| R         | Material constant|
|C<sub>01</sub>| R         | Material constant|
|D             | R         | Material constant|
|C<sub>42</sub>| R         | Material constant|
|C<sub>43</sub>| R         | Material constant|

####### In case of `TYPE = USER`

```
(2nd line-10th line) v1, v2, v3, v4, v5, v6, v7, v8, v9, v10
```

