##### `!CONTACT_PARAM` (2-14)

Specify contact scan parameter set.

###### Parameter

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| NAME         | C    | Contact scan parameter set name (required) |

** 2nd line **

Specify clearance values in in-surface directions.

```
(2nd line) CLEARANCE, CLR_SAME_ELEM, CLR_DIFFLPOS, CLR_CAL_NORM
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| CLEARANCE     | R    | ordinary clearance (Default: 1e-4)           |
| CLR_SAME_ELEM | R    | clearance for already-in-contct elems (loosen to avoid moving too easily) (Default: 5e-3) |
| CLR_DIFFLPOS  | R    | clearance to be recognized as different position (loosen to avoid oscillation) (Default: 1e-2)     |
| CLR_CAL_NORM  | R    | clearance used when calculating surface normal (Default: 1e-40                |

** 3rd line **

Specify clearance values in directions vertical to the surface.

```
(3rd line) DISTCLR_INIT, DISTCLR_FREE, DISTCLR_NOCHECK, TENSILE_FORCE, BOX_EXP_RATE
```

|Parameter Name|Attribution|Contents|
|--------------|-----------|--------|
| DISTCLR_INIT    | R    | dist clearance for initial scan (Default: 1e-6)       |
| DISTCLR_FREE    | R    | dist clearance for free nodes (wait until little penetration to be judged as contact) (Default: -1e-6) |
| DISTCLR_NOCHECK | R    | dist clearance for skipping distance check for nodes already in contact (big value to keep contact because contact-to-free is judged by tensile force) (Default: 1.0) |
| TENSILE_FORCE   | R    | tensile force to be judged as free node (Default: -1e-8)                 |
| BOX_EXP_RATE    | R    | expansion rate of the box used for contact scan (the smaller the faster, the bigger the safer) (Default: 1.05) |


###### Example of Use

With the same settings as the default settings

```
!CONTACT_PARAM, NAME=CPARAM1
1.0e-4,  5.0e-3,  1.0e-2,  1.0e-4
1.0e-6, -1.0e-6,  1.0,    -1.0e-8,  1.05
```

