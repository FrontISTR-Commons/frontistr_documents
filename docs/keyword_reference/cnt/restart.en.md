##### `!RESTART` (1-8)

Controls the writing of the restart file. When not specified, the restart file can not be written.

###### Parameter

```
FREQUENCY = n        :step interval of output (Default: 0)
n > 0    :Output for each n step
n < 0    :First, reads the restart file, then outputs for each n step
```

###### Example of Use

```
!RESTART, FREQUENCY=-2
```

