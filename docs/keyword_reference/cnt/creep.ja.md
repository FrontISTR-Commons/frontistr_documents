##### `!CREEP` (2-2-5)

クリープ材料の定義

`!CREEP`を定義するときは、同じ`!MATERIAL`の中で`!ELASTIC`も同時に定義しなければならない。

###### パラメータ

```
TYPE = NORTON (Default値)
DEPENDENCIES = 0 (Default値) / 1
```

** 2行目以降 **

```
(2行目) A, n, m, Tempearature
```

| 変数名      | 属性 | 内容     |
|-------------|------|----------|
| A           | R    | 材料係数 |
| n           | R    | 材料係数 |
| m           | R    | 材料係数 |
| Tempearture | R    | 温度(`DEPENDENCIES=1`の時に必要) |
