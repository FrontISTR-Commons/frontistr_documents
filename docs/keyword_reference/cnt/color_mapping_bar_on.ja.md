##### `!color_mapping_bar_on`, `!scale_marking_on`, `!num_of_scales` (P2-16, P2-17, P2-18)

```
!color_mapping_bar_on : color mapping barの表示有無を指定する。
                         0: off 1: on (省略値: 0)

!scale_marking_on     : color mapping barのメモリの有無を指定する
                         0: off 1: on (省略値: 0)

!num_of_scales        : メモリの数を指定する。
                         (省略値: 3)
```

![color_mapping_barの表示の例](media/analysis05_16.png){.center width="80%"}

図7.4.14 `color_mapping_bar`の表示の例
