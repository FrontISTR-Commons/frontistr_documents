##### `!ORIENTATION` (1-10)

Definition of local coordinate system

###### Parameter

```
NAME = Name of local coordinate system
DEFINITION = COORDINATES (Default)/NODES
```

##### 2nd Line of later

- In case of `DEFINTION=COORDINATES`

```
(2nd line or later) a1, a2, a3, b1, b2, b3, c1, c2, c3
```

- In case of `DEFINTIION=NODES`

```
(2nd line or later) a, b, c
```

|Parameter Name|Attributions|Contents                      |
|--------------|------------|------------------------------|
|a1, a2, a3    |R           |coodinate of point a          |
|b1, b2, b3    |R           |coodinate of point b          |
|c1, c2, c3    |R           |coodinate of point c          |
|a,b,c         |I           |Node ID of a,b,c, respectively|

![Analysis Control Data](media/analysis05_02.png){.center width="50%"}


