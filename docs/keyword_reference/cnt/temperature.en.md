##### `!TEMPERATURE` (2-9)

Specification of nodal temperature used for thermal stress analysis

###### Parameter

```
READRESULT = Number of result steps of heat conduction analysis.
When specified, the temperature is sequentially input from
the results file of the heat conduction analysis,
and the 2nd line and later will be disregarded.
SSTEP      = First step number that performs the reading
of the heat conduction analysis results (Default: 1)
INTERVAL   = Step interval that performs the reading
of the heat conduction analysis results (Default: 1)
READTYPE   = STEP(Default) / TIME
When TIME is specified, analysis time of the stress
analysis is synchronized with the heat conduction
analysis (value of INTERVAL is ignored, and the
temperature is linearly interpolated from results of the
heat conduction analysis right before and after the
current analysis time)
```

When unsteady heat conduction analysis using auto time increment was
performed, and the results were output at specified time points using
!TIME_POINTS, READTYPE=TIME needs to be specified because the step
interval of the results is not constant.

** 2nd line or later **

```
(2nd line) NODE_ID, Temp_Value
```

|Parameter Name|Attributions|Contents                  |
|--------------|------------|--------------------------|
| NODE_ID      | I/C        |Node ID or node group name|
| Temp_Value   | R          |Temperature (Default: 0)  |

###### Example of Use

```
!TEMPERATURE
1, 10.0
2, 120.0
3, 330.0
!TEMPERATURE
ALL, 20.0
!TEMPERATURE, READRESULT=1, SSTEP=1
```

