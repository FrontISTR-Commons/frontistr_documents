##### `!CONTACT` (2-8)

Definition of contact conditions

###### Parameter

```
GRPID       = Boundary conditions group ID
INTERACTION = SSLID (Infinitesimal slip contact, Default) / FSLID (Finite slip contact) / TIED (Tied)
NTOL        = Contact normal direction convergence threshold (Default: 1.e-5)
TTOL        = Contact tangential direction convergence threshold (Default: 1.e-3)
NPENALTY    = Contact normal direction Penalty (Default: stiffness matrix 1.e3)
TPENALTY    = Contact tangential direction Penalty (Default: 1.e3)
CONTACTPARAM = Contact scan parameter set name（specified by `!CONTACT_PARAM, NAME`）
```

**2nd line or later**

####### Using `INTERACTION = SSLID, FSLID`

```
(2nd line) PAIR_NAME, fcoef, factor
```

|Parameter Name|Attributions|Contacts|
|--------------|------------|-----------------------------------------------|
| PAIR_NAME    | C          |Contact pair name (Defined in `!CONTACT_PAIR`) |
| fcoef        | R          |Friction coefficient (Default: 0.0)            |
| factor       | R          |Friction penalty stiffness                     |

####### Using `INTERACTION = TIED`

```
(2nd line) PAIR_NAME
```

|Parameter Name|Attributions|Contacts|
|-----------|------|--------------------------------------|
| PAIR_NAME | C    |Contact pair name (Defined in `!CONTACT_PAIR`) |


###### Example of Use

```
!CONTACT_ALGO, TYPE=SLAGRANGE
!CONTACT, GRPID=1, INTERACTION=FSLID
CP1, 0.1, 1.0e+5
```

####### Note

- if `CONTACTPARAM` is specified, the destination `!CONTACT_PARAM` must be defined prior to the `!CONTACT` card. If this parameter is omitted, the default contact scan parameter set is used.

