##### `!SRADIATE` (4-9)

Definition of radiation factor by surface group

###### Parameter

```
AMP1 = Radiation factor history table name (specified in !AMPLITUDE)
AMP2 = Ambient temperature history table name (specified in !AMPLITUDE)
```

** 2nd line or later **

```
(2nd line) SURFACE_GRP_NAME, Value, Sink
```

|Parameter Name| Attributions| Contents|
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | Surface group name |
| Value            | R    | Radiation factor |
| Sink             | R    | Ambient temperature |

###### Example of Use

```
!SRADIATE
RSURF, 1.0E-9, 800.0
!SRADIATE, AMP2=TSRAD
RSURF, 1.0E-9, 1.0
```

