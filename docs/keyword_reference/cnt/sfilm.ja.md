##### `!SFILM` (4-7)

面グループによる熱伝達係数の定義

###### パラメータ

```
AMP1 = 熱伝達係数履歴テーブル名 (!AMPLITUDE で指定)
AMP2 = 雰囲気温度履歴テーブル名 (!AMPLITUDE で指定)
```

** 2行目以降 **

```
(2行目) SURFACE_GRP_NAME, Value, Sink
```

| 変数名           | 属性 | 内容         |
|------------------|------|--------------|
| SURFACE_GRP_NAME | C    | 面グループ名 |
| Valu             | R    | 熱伝達率     |
| Sink             | R    | 雰囲気温度   |

###### 使用例

```
!SFILM
  SFSURF, 1.0, 800.0
!SFILM, AMP1=TSFILM, AMP2=TFILM
  SFSURF, 1.0, 1.0
```
