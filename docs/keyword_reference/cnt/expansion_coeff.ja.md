##### `!EXPANSION_COEFF`(2-2-7)

線膨張係数の定義

ここで入力する線膨張係数は、各温度における線膨張係数\(\alpha\)の値そのものではなく、
次式に示す、参照温度\(T_{ref}\)から各温度\(T\)までの区間平均値である。

\begin{equation}
\overline{\alpha}(T)=\frac{1}{T-T_{ref}} \int_{T_{ref}}^T \alpha(T) dT
\end{equation}

###### パラメータ

```
TYPE = 材質タイプ
       ISOTROPIC(等方性:Default) / ORTHOTROPIC(直交異方性)
DEPENDENCIES = 0(Default値) / 1
```

** 2行目以降 **

####### `TYPE=ISOTROPIC`の場合

```
(2行目) expansion, Temperature
```

####### `TYPE=ORTHOTROPIC`の場合

```
(2行目) α11, α22, α33, Temperature
```

| 変数名        | 属性 | 内容                           |
|---------------|------|--------------------------------|
| expansion     | R    | 線膨張係数                     |
| α11, α22, α33 | R    | 線膨張係数                     |
| Tempearture   | R    | 温度(DEPENDENCIES=1の時に必要) |
