
Specifies the mesh data file.

```
!MESH, NAME=<name>, TYPE=<type> [,optional parameter]
fileheader

#Example of Use
!MESH, NAME=fstrMSH, TYPE=HECMW-DIST, REFINE=1
Mesh.in
```

| Parameter |                                            |
|:----------|:-------------------------------------------|
| `NAME`    | Identifier (mandatory)                     |
| `TYPE`    | Mesh type (mandatory)                      |
| `IO`      | Input/output specification (omissible)     |
| `REFINE`  | Mesh subdivision specification (arbitrary) |

| Parameter Name | Parameter Value | Contents                       |
|:---------------|:----------------|:-------------------------------|
| `NAME`         | `fstrMSH`       | Solver input data              |
|                | `part_in`       | Partitioner input data         |
|                | `part_out`      | Partitioner output data        |
| `TYPE`         | `HECMW-DIST`    | HEC-MW distribution mesh data  |
|                | `HECMW-ENTIRE`  | HEC-MW single domain mesh data |
| `IO`           | `IN`            | For input (default)            |
|                | `OUT`           | For output                     |
| `REFINE`       | &lt;integer&gt; | Number of mesh subdivisions    |


|Parameter Name | Contents                                                                                         |
|---------------|--------------------------------------------------------------------------------------------------|
| fileheader    | Header of the mesh data file name (both the relative path and the absolute path can be specified. When the relative path is specified, it becomes the path from the current directory.)|

> Note:
>
> The existence of IO parameters, or parameter values will have no affect on others. When the `TYPE` is `HECMW-DIST`, the end of the file name ".&lt;rank&gt;" is excluded for the file header specified in the data line.

