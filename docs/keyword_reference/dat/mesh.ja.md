
メッシュデータファイルを指定する。

```
!MESH, NAME=<name>, TYPE=<type> [,optional parameter]
fileheader

#使用例
!MESH, NAME=fstrMSH, TYPE=HECMW-DIST, REFINE=1
Mesh.in
```

| パラメータ  |                            |
|:------------|:---------------------------|
| `NAME`      |識別子（必須）              |
| `TYPE`      |メッシュタイプ（必須）      |
| `IO`        |入出力指定（省略可）        |
| `REFINE`    |メッシュ細分化指定（任意）  |

| パラメータ名 | パラメータ値    | 内 容                        |
|:-------------|:----------------|:-----------------------------|
| `NAME`       | `fstrMSH`       | Solver入力データ             |
|              | `part_in`       | Partitioner入力データ        |
|              | `part_out`      | Partitioner出力データ        |
| `TYPE`       | `HECMW-DIST`    | HEC-MW分散メッシュデータ     |
|              | `HECMW-ENTIRE`  | HEC-MW単一領域メッシュデータ |
| `IO`         | `IN`            | 入力用（デフォルト）         |
|              | `OUT`           | 出力用                       |
| `REFINE`     | &lt;integer&gt; | メッシュ細分化回数           |


| 変数名     |内容      |
|------------|----------|
| fileheader |メッシュデータファイル名のヘッダー 相対パス、絶対パス共に指定可能。相対パスの場合はカレントディレクトリからのパスとなる |

注意

`IO`パラメータの有無、パラメータ値は動作に何も影響を与えない。

`TYPE`が`HECMW-DIST`の場合、データ行に指定するfileheaderはファイル名末尾の「.&lt;rank&gt;」を除いたものである。

