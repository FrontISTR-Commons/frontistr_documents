
Specifies the analysis control data file.

```
!CONTROL,NAME=<name>
file

#### Example of Use

!CONTROL, NAME=fstrCNT
myctrl.dat
```

| Parameter |                        |
|:----------|:-----------------------|
| `NAME`    | Identifier (mandatory) |

| Parameter Name | Parameter Value | Contents              |
|:---------------|:----------------|:----------------------|
| `NAME`         | `fstrCNT`       | Analysis control data |

| Parameter Name |Contents                                                          |
|----------------|------------------------------------------------------------------|
| file           |Analysis control data file name (both the relative path and the absolute path can be specified. When the relative path is specified, it becomes the path from the current directory.)|


