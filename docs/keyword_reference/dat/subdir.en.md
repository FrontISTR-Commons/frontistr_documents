
Specifies storing input/output files into subdirectories

```
!SUBDIR, ON [,optional parameter]

# Example of Use
!SUBDIR, ON, LIMIT=8000
```

| Parameter |                             |
|:----------|:----------------------------|
| `ON`      | Enable (mandatory)          |
| `LIMIT`   | Number of files (omissible) |

| Parameter Name | Parameter Value | Contents                                             |
|:---------------|:----------------|:-----------------------------------------------------|
| `ON`           | N/A             |                                                      |
| `LIMIT`        | &lt;integer&gt; | Maximum number of files per directory (default:5000) |

> Note:
>
> The input/output data which are comprised of plural files are automatically stored into each subdirectory by this definition.
> Furthermore, if the number of ranks exceeds “`LIMIT`”, those files are divided to subdirectoris such as `TRUNK0`, `TRUNK1`.




