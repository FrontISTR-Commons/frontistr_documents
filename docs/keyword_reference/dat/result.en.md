
Specifies the analysis results data file.

```
!RESULT, NAME=<name> [,optional parameter]
fileheader

# Example of Use
!RESULT, NAME=fstrRES, IO=OUT, TYPE=BINARY
result.out
```

| Parameter |                                        |
|:----------|:---------------------------------------|
| `NAME`    | Identifier (mandatory)                 |
| `IO`      | Input/output specification (mandatory) |
| `TYPE`    | Output format (omissible)              |

| Parameter Name | Parameter Value | Contents                                         |
|:---------------|:----------------|--------------------------------------------------|
| `NAME`         | `fstrRES`       | Solver output data, Visualizer input data        |
|                | `fstrTEMP`      | Temperature input data (Result of heat analysis) |
|                | `vis_out`       | Visualizer output data                           |
| `IO`           | `IN`            | For input                                        |
|                | `OUT`           | For output                                       |
| `TYPE`         | `TEXT`          | Text format (default)                            |
|                | `BINARY`        | Binary format                                    |

|Parameter Name|Contents |
|:-------------|:--------|
| fileheader   |Header of the analysis results data file name (both the relative path and the absolute path can be specified. When the relative path is specified, it becomes the path from the current directory.)|

> Note:
>
> The file name created by this definition is the file header+.&lt;rank&gt;.


