
Specifies the restart data file.

```
!RESTART, NAME=<name>, IO=<io>
fileheader

# Example of Use
!RESTART, NAME=restart-in, IO=IN
restart.in
```

| Parameter | Contents                               |
|:----------|:---------------------------------------|
| `NAME`    | Identifier (mandatory)                 |
| `IO`      | Input/output specification (mandatory) |

| Parameter Name | Parameter Value | Contents               |
|:---------------|:----------------|:-----------------------|
| `NAME`         | &lt;name&gt;    | Identifier             |
| `IO`           | IN              | For input              |
|                | OUT             | For output             |
|                | INOUT           | Common to input/output |

|Parameter Name|Contents|
|:-------------|--------|
|fileheader    |Header of the restart data file name (both the relative path and the absolute path can be specified. When the relative path is specified, it becomes the path from the current directory.)|

> Note:
>
> The file name created by this definition is the file header+.&lt;rank&gt;.


