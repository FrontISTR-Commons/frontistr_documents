# FrontISTR_documents

## 概要

- ファイルはmarkdown形式で記述
- [mkdocs](http://www.mkdocs.org)でビルドするとDocumentation サイト（英語版・日本語版）を作成することができる

## mkdocs のセットアップ

[mkdocs](http://www.mkdocs.org)本体およびPython-Markdown, Math extension for Python-Markdown, static i18n pluginプラグインをインストールする
~~~
% pip install mkdocs mkdocs-material python-markdown-math mkdocs-static-i18n
~~~

## ビルド
ルートディレクトリでビルドコマンドを実行する。
~~~
% mkdocs build --clean
~~~
siteフォルダが生成されるので、中身一式を取得してupする。

